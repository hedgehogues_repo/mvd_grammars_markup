import os

from internal.algorithms.combiner import CombinerAlgorithm
from internal.algorithms.grammars import GrammarAlgorithm
from internal.enums import ZonesKeys
from internal.grammatics.attached_to import ATTACHED_TO
from internal.grammatics.cusp import CUSP
from internal.grammatics.other.time import TIME
from internal.grammatics.other.utils import ADDRESS
from internal.grammatics.reported_at import REPORTED_AT
from internal.grammatics.reported_by import REPORTED_BY, ANON_REPORTED_BY
from internal.grammatics.run_by import RUN_BY
from internal.grammatics.source import GLONASS, SOURCE
from internal.grammatics.text import TEXT
from internal.grammatics.when import WHEN
from internal.grammatics.where import ORGANISATION_NAME, WHERE
from internal.grammatics.who import WHO
from internal.pipelines.data import Data
from internal.pipelines.io.fileseparator import FileSeparator
from internal.pipelines.io.io import DescriptionFileWriter, DataFileReader
from internal.pipelines.io.merger import Merger
from internal.pipelines.pipeline import Pipeline


def compose(path, in_file, out_file, processes, print_step, max_samples=None):
    files = FileSeparator(in_path=path + in_file, out_path=path, files_proportion=processes,
                          max_samples=max_samples).separate()
    data = [
        Data(
            description_reader=None,
            description_writer=DescriptionFileWriter(path + 'out_' + str(pr_num) + '.json'),
            data_reader=DataFileReader(file),
            data_writer=None
        ) for pr_num, file in enumerate(files)
    ]

    algorithms = [
        ('WHO', GrammarAlgorithm(grammar=WHO, key=ZonesKeys.WHO)),
        ('ATTACHED_TO', GrammarAlgorithm(grammar=ATTACHED_TO, key=ZonesKeys.ATTACHED_TO)),
        ('CUSP', GrammarAlgorithm(grammar=CUSP, key=ZonesKeys.CUSP)),
        ('REPORTED_AT', GrammarAlgorithm(grammar=REPORTED_AT, key=ZonesKeys.REPORTED_AT)),
        ('ANON_REPORTED_BY', GrammarAlgorithm(grammar=ANON_REPORTED_BY, key=ZonesKeys.ANON_REPORTED_BY)),
        ('REPORTED_BY', GrammarAlgorithm(grammar=REPORTED_BY, key=ZonesKeys.REPORTED_BY)),
        ('RUN_BY', GrammarAlgorithm(grammar=RUN_BY, key=ZonesKeys.RUN_BY)),
        ('GLONASS', GrammarAlgorithm(grammar=GLONASS, key=ZonesKeys.GLONASS)),
        ('SOURCE', GrammarAlgorithm(grammar=SOURCE, key=ZonesKeys.SOURCE)),
        ('TEXT', GrammarAlgorithm(grammar=TEXT, key=ZonesKeys.TEXT)),
        ('WHEN', GrammarAlgorithm(grammar=WHEN, key=ZonesKeys.WHEN)),
        ('WHERE', GrammarAlgorithm(grammar=WHERE, key=ZonesKeys.WHERE)),
        ('ORGANISATION_NAME', GrammarAlgorithm(grammar=ORGANISATION_NAME, key=ZonesKeys.ORGANISATION_NAME)),
        ('ADDRESS', GrammarAlgorithm(grammar=ADDRESS, key=ZonesKeys.ADDRESS)),
        ('TIME', GrammarAlgorithm(grammar=TIME, key=ZonesKeys.TIME)),
        ('Combiner', CombinerAlgorithm()),
    ]

    pipeline = Pipeline(data=data, print_step=print_step)
    pipeline.set_algorithms(algorithms)
    pipeline.start()

    Merger(
        in_paths=[path + 'out_' + os.path.basename(files[file_num]) for file_num in range(processes)
                  if file_num < len(files)],
        out_file=path + out_file + '_',
        remove=True
    ).merge()
    for file in files:
        os.remove(file)

    fd_out = open(path + out_file, 'w')
    for ind, line in enumerate(open(path + out_file + '_')):
        if ind % 3 == 0:
            fd_out.write(str(ind // 3 + 1) + '. ' + line)
        else:
            fd_out.write(line)

    os.remove(path + out_file + '_')
