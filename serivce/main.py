from serivce.compose import compose
import time

path = '../data/'
in_file = 'test.txt'
out_file = 'result.txt'
processes = 7
print_step = 25
max_samples = 1000
start_time = time.time()
compose(path, in_file, out_file, processes, print_step, max_samples)
print("--- %s seconds ---" % (time.time() - start_time))
