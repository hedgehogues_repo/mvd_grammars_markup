from enum import IntEnum


class ZonesKeys(IntEnum):
    ATTACHED_TO = 0
    CUSP = 1
    REPORTED_AT = 2
    ANON_REPORTED_BY = 3
    REPORTED_BY = 4
    RUN_BY = 5
    GLONASS = 6
    SOURCE = 7
    TEXT = 8
    WHEN = 9
    WHERE = 10
    ORGANISATION_NAME = 11
    WHO = 12
    ADDRESS = 13
    TIME = 14
    RESULT_JSON = 15
