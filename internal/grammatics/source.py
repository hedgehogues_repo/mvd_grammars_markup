from yargy import rule, or_
from yargy.predicates import eq, normalized, in_
from yargy.tokenizer import QUOTES

from internal.interpretations import source
from internal.grammatics.other.time import TIME
from internal.grammatics.other.utils import SEP


__GLONASS = rule(
    or_(
        normalized('глонасс'),
        normalized('глонас'),
    ),
)
__SEP = or_(
    SEP,
    rule(eq('+')),
    rule(in_(QUOTES)),
)
__CODE = or_(
    eq('112'),
    eq('04'),
    eq('03'),
    eq('02'),
)
SOURCE = or_(
    TIME,
    rule(
        __CODE.interpretation(source.source),
    ),
    rule(
        in_(QUOTES),
        __CODE.interpretation(source.source),
        in_(QUOTES),
    ),
).interpretation(source)
GLONASS = rule(
    in_(QUOTES).optional(),
    rule(
        __GLONASS,
        __SEP.optional(),
        __CODE.optional(),
    ).interpretation(source.source),
    in_(QUOTES).optional(),
).interpretation(source)
