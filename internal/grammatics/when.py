from yargy import rule, or_
from yargy.predicates import gram, dictionary

from internal.grammatics.other.time import TIME
from internal.grammatics.other.time_interval import TIME_INTERVAL
from internal.grammatics.other.utils import DATE, ADDRESS, SEP
from internal.grammatics.reported_at import REPORTED_AT
from internal.interpretations import when
from internal.utils import generate_all_permutations

__TIME_RULE = rule(
    or_(
        TIME.interpretation(when.time),
        TIME_INTERVAL.interpretation(when.interval),
    )
)

__PREFIX_SUFFIX = rule(
    dictionary(['по', 'в', 'около', 'на', 'рядом', 'возле']).optional(),
    ADDRESS.interpretation(when.address),
)
__PREFIX = rule(
    or_(
        rule(
            __PREFIX_SUFFIX,
            SEP,
        ),
        __PREFIX_SUFFIX,
    ),
)

__SUFFIX = rule(
    or_(
        rule(
            SEP,
            __PREFIX_SUFFIX,
        ),
        __PREFIX_SUFFIX,
    ),
)

__SEP = rule(
    gram('PREP').optional(),
    SEP.optional(),
    gram('PREP').optional(),
)

WHEN = or_(
    REPORTED_AT,
    rule(
        __PREFIX.optional(),
        or_(
            rule(
                gram('PREP').optional(),
                SEP.optional(),
                __TIME_RULE,
            ),
            *generate_all_permutations([DATE.interpretation(when.date), __TIME_RULE], sep=__SEP),
        ),
        __SUFFIX.optional(),
    )
).interpretation(when)
