from yargy import rule
from yargy.predicates import gram, normalized

from internal.grammatics.other.utils import SEP, WORD
from internal.interpretations import text

TEXT = rule(
    rule(
        normalized('по'),
        normalized('факт'),
    ),
    rule(
        SEP.optional(),
        WORD,
        SEP.optional(),
        rule(gram('PREP').optional()),
        SEP.optional(),
        WORD.optional(),
        SEP.optional(),
        rule(gram('PREP').optional()),
        SEP.optional(),
        WORD.optional(),
    ).interpretation(text.text.inflected()),
).interpretation(text)
