from yargy import rule, or_
from yargy.predicates import gram, dictionary

from internal.grammatics.other.time import TIME
from internal.grammatics.other.utils import SEP, NAME, ABBR, INT
from internal.interpretations import run_by_item, run_by
from internal.utils import generate_all_subsets

__ACTION = rule(
    dictionary(['выезд', 'выезжали', 'выехали', 'работали', 'работ', 'работал', 'занимался', 'занимались', 'заним'])
)
__ABBR_OR_INT = rule(
    or_(
        *generate_all_subsets([ABBR, INT], SEP.optional()),
    ),
)
__NAME_INTERP = NAME.interpretation(run_by_item.name)
__ABBR_OR_INT_INTERP = __ABBR_OR_INT.interpretation(run_by_item.position)
NAME_POSITION = rule(
    or_(
        *generate_all_subsets([__NAME_INTERP, __ABBR_OR_INT_INTERP], SEP.optional()),
    ),
).interpretation(run_by_item)
__NAME_REPEATABLE = rule(
    rule(
        NAME_POSITION.interpretation(run_by.items),
        SEP.optional(),
    ).repeatable(),
)
__TIME_PREP = rule(
    gram('PREP').optional(),
    TIME.interpretation(run_by.time),
)
__NAME_PREP = rule(
    gram('PREP').optional(),
    __NAME_REPEATABLE,
)
RUN_BY = rule(
    __ACTION,
    SEP.optional(),
    SEP.optional(),
    __TIME_PREP.optional(),
    SEP.optional(),
    __NAME_PREP.optional(),
    SEP.optional(),
    __TIME_PREP.optional(),
).interpretation(run_by)
