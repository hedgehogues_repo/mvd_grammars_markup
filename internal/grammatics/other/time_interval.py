from yargy import rule, or_
from yargy.predicates import gram, normalized

from internal.grammatics.other.time import TIME
from internal.grammatics.other.utils import DATE, SEP
from internal.interpretations import interval
from internal.utils import eq_lower

TIME_INTERVAL = rule(
    eq_lower('в'),
    normalized('период'),
    normalized('время').optional(),
    SEP.optional(),
    gram('PREP').optional(),
    SEP.optional(),
    or_(
        TIME,
        DATE,
    ).interpretation(interval.from_),
    SEP.optional(),
    gram('PREP').optional(),
    SEP.optional(),
    or_(
        TIME,
        DATE,
    ).interpretation(interval.to),
).interpretation(interval)
