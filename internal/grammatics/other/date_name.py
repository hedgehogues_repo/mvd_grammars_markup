from yargy import rule, or_

from internal.interpretations import person
from internal.grammatics.other.birthday import BIRTHDAY
from internal.grammatics.other.utils import NAME, DATE
from internal.utils import get_resolved_shorts

DATE_NAME_RULE = rule(
    get_resolved_shorts('citizen').optional(),
    or_(
        NAME.interpretation(person.name),
        rule(
            NAME.interpretation(person.name),
            DATE.interpretation(person.birthday),
        ),
        rule(
            DATE.interpretation(person.birthday),
            NAME.interpretation(person.name),
        ),
        BIRTHDAY,
    ),
).interpretation(person)
