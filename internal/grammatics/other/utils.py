from yargy import rule, or_, and_, not_
from yargy.predicates import eq, dictionary, is_capitalized, type

from internal.interpretations import date, name, address
from internal.utils import eq_lower, get_resolved_shorts, LengthGte, LengthLte

# TODO: согласно доке, interpretation(name.name).interpretation(name) должна падать, но работает валидно
NAME = rule(
    eq('[').optional(),
    eq('[').optional(),
    eq_lower('name'),
    eq(']').optional(),
    eq(']').optional(),
).interpretation(name.name).interpretation(name)

# TODO: согласно доке, interpretation(name.name).interpretation(name) должна падать, но работает валидно
DATE = rule(
    eq('[').optional(),
    eq('[').optional(),
    eq_lower('date'),
    eq(']').optional(),
    eq(']').optional(),
).interpretation(date.date).interpretation(date)

# DATE_FIRST = rule(
#     and_(
#         IsStartLine(),
#         eq('[').optional(),
#         eq('[').optional(),
#         to_lower('date'),
#         eq(']').optional(),
#         eq(']').optional(),
#     ),
# )

# TODO: согласно доке, interpretation(name.name).interpretation(name) должна падать, но работает валидно
ADDRESS = rule(
    or_(
        get_resolved_shorts('floor'),
        get_resolved_shorts('entrance'),
        get_resolved_shorts('house'),
        get_resolved_shorts('flat'),
        get_resolved_shorts('street'),
        get_resolved_shorts('village'),
        get_resolved_shorts('city'),
    ).interpretation(address.prefix).optional(),
    rule(
        eq('[').optional(),
        eq('[').optional(),
        or_(
            eq_lower('adress'),
            eq_lower('adres'),
            eq_lower('addres'),
            eq_lower('address'),
        ),
        eq(']').optional(),
        eq(']').optional(),
    ).interpretation(address.address),
).interpretation(address)

SEP = rule(
    dictionary({'.', ',', '-', '/', ':'}),
)

ABBR = rule(
    and_(
        LengthGte(2),
        LengthLte(4),
        is_capitalized(),
        not_(type('INT')),
    ),
)

INT = rule(
    and_(
        LengthGte(2),
        LengthLte(5),
        type('INT'),
    ),
)

WORD = rule(
    and_(
        LengthLte(30),
        LengthGte(2),
    ),
)
