from yargy import rule
from yargy.predicates import gram

from internal.interpretations import address
from internal.grammatics.other.utils import ADDRESS
from internal.utils import get_resolved_shorts

ADDRESS_RULE = rule(
    get_resolved_shorts('living').optional(),
    gram('PREP').optional(),
    ADDRESS,
).interpretation(address)
