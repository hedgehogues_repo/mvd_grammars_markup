from yargy import rule, or_
from yargy.predicates import eq, normalized

from internal.interpretations import person
from internal.grammatics.other.utils import DATE, SEP, NAME
from internal.utils import eq_lower


__YEAR = rule(
    or_(
        eq_lower('г'),
        normalized('год'),
    ),
)
__BIRTH = rule(
    or_(
        eq_lower('р'),
        eq_lower('рожд'),
        normalized('рождение'),
    ),
)
__GR = rule(
    SEP.optional(),
    or_(
        rule(
            __YEAR,
            SEP.optional(),
        ),
        rule(
            __YEAR,
            SEP.optional(),
            __BIRTH,
        ),
        rule(
            SEP.optional(),
            __BIRTH,
        ),
        rule(
            eq_lower('гр'),
        )
    ),
    eq('.').optional(),
)
BIRTHDAY = rule(
    or_(
        rule(
            NAME.interpretation(person.name),
            SEP.optional(),
            DATE.interpretation(person.birthday),
            eq(',').optional(),
            __GR.optional(),
        ),
        rule(
            DATE.interpretation(person.birthday),
            eq(',').optional(),
            __GR,
        ),
    ),
).interpretation(person)
