from yargy import rule, or_, and_
from yargy.predicates import gte, lte, dictionary, normalized, eq

# TODO: Нет правил для времени, написанного буквами
from internal.interpretations import time

__HOURS = rule(
    and_(
        gte(0),
        lte(23),
    )
).interpretation(time.hours.custom(int))
__MINUTES = rule(
    and_(
        gte(0),
        lte(59),
    ),
).interpretation(time.minutes.custom(int))

__HOUR_WORD = rule(
    normalized('час'),
    eq('.').optional(),
)
__MINUTES_WORD = rule(
    or_(
        or_(
            normalized('минута'),
            eq('м'),
        ),
        eq('мин')
    ),
    eq('.').optional(),
)

__LAST_TOKEN = rule(
    or_(
        __HOUR_WORD.optional(),
        __MINUTES_WORD.optional(),
    )
)
__SEP = rule(
    dictionary({'.', ':', '-', ','})
)

TIME = rule(
    __HOURS,
    or_(
        __HOUR_WORD,
        __SEP.optional(),
    ),
    __MINUTES,
    __LAST_TOKEN,
).interpretation(time)
