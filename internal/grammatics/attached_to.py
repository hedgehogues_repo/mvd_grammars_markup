from yargy import rule
from yargy.predicates import gram

from internal.grammatics.cusp import CUSP
from internal.grammatics.other.utils import SEP
from internal.interpretations import attach_to
from internal.utils import get_resolved_shorts

ATTACHED_TO = rule(
    get_resolved_shorts('attached_to'),
    SEP.optional(),
    gram('PREP').optional(),
    gram('NOUN').optional(),
    SEP.optional(),
    CUSP.interpretation(attach_to.cusp),
).interpretation(attach_to)
