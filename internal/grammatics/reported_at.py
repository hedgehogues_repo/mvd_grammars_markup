from yargy import rule, or_
from yargy.predicates import gram, normalized

from internal.grammatics.other.utils import DATE, SEP
from internal.grammatics.other.time import TIME
from internal.interpretations import reported_at
from internal.utils import get_resolved_shorts, generate_all_subsets

__DELIVER = get_resolved_shorts('deliver').interpretation(reported_at.report.inflected())
__REPORT = get_resolved_shorts('report').interpretation(reported_at.report.inflected())
__SUFFIX_RULE = rule(
    SEP.optional(),
    gram('PREP').optional(),
)

__SEP = rule(
    SEP.optional(),
    or_(
        *(
            generate_all_subsets([__REPORT, __DELIVER], SEP.optional(), suffix_rule=__SUFFIX_RULE) +
            [gram('PREP').optional()]
        ),
    ),
    SEP.optional(),
)

REPORTED_AT = rule(
    DATE.interpretation(reported_at.date),
    normalized('год').optional(),
    __SEP,
    TIME.interpretation(reported_at.time),
).interpretation(reported_at)
