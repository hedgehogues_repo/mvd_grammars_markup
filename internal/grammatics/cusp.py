from yargy import rule, or_, and_
from yargy.predicates import gte, lte, normalized, eq

from internal.grammatics.other.utils import SEP
from internal.interpretations import cusp
from internal.utils import eq_lower, is_letter, LengthGte, LengthLte, generate_all_subsets

__INPUT = rule(
    or_(
        eq_lower('вход'),
        eq_lower('вх'),
        normalized('входной'),
    ),
)
__PREFFIX_SUFFIX_LETTERS = and_(
    is_letter,
    LengthGte(1),
    LengthLte(3),
)
__NUM = rule(
    rule(
        __PREFFIX_SUFFIX_LETTERS,
        SEP,
    ).optional(),
    and_(
        gte(1),
        lte(50000),
    ),
    rule(
        SEP,
        __PREFFIX_SUFFIX_LETTERS,
    ).optional(),
)
__CUSP_TEXT = rule(
    or_(
        *generate_all_subsets([rule(eq_lower('кусп')), __INPUT], SEP.optional()),
    )
)
CUSP = rule(
    __CUSP_TEXT,
    SEP.optional(),
    or_(
        eq('#'),
        eq('№'),
    ).optional(),
    SEP.optional(),
    __NUM.interpretation(cusp.id),
).interpretation(cusp)
