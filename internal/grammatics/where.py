from yargy import rule, or_
from yargy.predicates import dictionary
from yargy.tokenizer import LEFT_QUOTES, GENERAL_QUOTES, RIGHT_QUOTES

from internal import templates
from internal.grammatics.other.utils import SEP, NAME, ADDRESS, WORD
from internal.interpretations import where, organisation, name
from internal.utils import get_resolved_shorts

__ORGANISATION_TYPE = rule(
    get_resolved_shorts('places').optional(),
    get_resolved_shorts('places'),
)
__ORGANISATION_TYPE_WITH_ABBR = or_(
    __ORGANISATION_TYPE.interpretation(organisation.type.inflected()),
    dictionary(templates['abbr']).interpretation(organisation.type.inflected()),
)
ORGANISATION_NAME = rule(
    or_(
        __ORGANISATION_TYPE_WITH_ABBR,
        rule(
            __ORGANISATION_TYPE_WITH_ABBR.optional(),
            SEP.optional(),
            dictionary(LEFT_QUOTES + GENERAL_QUOTES),
            NAME.interpretation(organisation.name),
            dictionary(RIGHT_QUOTES + GENERAL_QUOTES),
        ),
        rule(
            __ORGANISATION_TYPE_WITH_ABBR,
            SEP.optional(),
            NAME.interpretation(organisation.name),
        ),
        rule(
            __ORGANISATION_TYPE_WITH_ABBR,
            SEP.optional(),
            dictionary(LEFT_QUOTES + GENERAL_QUOTES),
            rule(
                WORD.optional(),
                WORD,
            ).interpretation(name.name.inflected()).interpretation(name).interpretation(organisation.name),
            dictionary(RIGHT_QUOTES + GENERAL_QUOTES),
        ),
    ),
).interpretation(organisation)

ADDRESS_NEAR = rule(
    dictionary(['по', 'в', 'около', 'на', 'рядом', 'возле', 'между', 'из']),
    ADDRESS.interpretation(where.address),
)

WHERE = or_(
    ADDRESS_NEAR,
    ORGANISATION_NAME.interpretation(where.organisation),
    rule(
        ORGANISATION_NAME.interpretation(where.organisation),
        SEP.optional(),
        WORD.optional(),
        SEP.optional(),
        WORD.optional(),
        SEP.optional(),
        rule(
            dictionary(['по', 'в', 'около', 'на', 'рядом', 'возле']).optional(),
            ADDRESS.interpretation(where.address),
        ),
    ),
    rule(
        rule(
            dictionary(['по', 'в', 'около', 'на', 'рядом', 'возле']).optional(),
            ADDRESS.interpretation(where.address),
        ),
        SEP.optional(),
        WORD.optional(),
        SEP.optional(),
        WORD.optional(),
        SEP.optional(),
        ORGANISATION_NAME.interpretation(where.organisation),
    ),
).interpretation(where)
