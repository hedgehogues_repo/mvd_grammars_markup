from yargy import rule, or_, and_, not_
from yargy.predicates import normalized, gram, dictionary, eq

from internal.grammatics.other.address import ADDRESS_RULE
from internal.grammatics.other.utils import SEP
from internal.grammatics.other.date_name import DATE_NAME_RULE
from internal.interpretations import who
from internal.utils import eq_lower

__NAMED = rule(
    gram('NUMR').optional(),
    dictionary(['осужденный', 'молодежь', 'сосед', 'несовершеннолетний', 'пьяный']),
)
__FAMILY = rule(
    dictionary(['муж', 'дочь', 'сын', 'жена', 'внук', 'внучка', 'зять', 'бабушка', 'дед', 'дедушка',
                'сожитель', 'сожительница', 'отчим', 'мачеха', 'брат', 'сестра']),
)
__ANYONE = rule(
    gram('NUMR').optional(),
    dictionary(['труп', 'мужчина', 'женщина', 'девушка', 'парень', 'мальчик', 'девочка', 'знакомый']),
)
__GROUP = rule(
    gram('NUMR').optional(),
    __NAMED,
)
__YOUNG = rule(
    normalized('несовершеннолетний'),
    __FAMILY,
)
__MAN = rule(
    or_(
        rule(
            gram('NUMR'),
            normalized('молодой'),
        ),
        rule(
            normalized('молодой')
        ),
        rule(
            gram('NUMR'),
        ),
    ).optional(),
    normalized('человек'),
)
__PERSON = rule(
    or_(
        __NAMED,
        __GROUP,
        __FAMILY,
        __MAN,
        __ANYONE,
        __YOUNG,
    ),
).interpretation(who.position.inflected())
__UNKNOW_PERSON = rule(
    gram('NUMR').optional(),
    or_(
        rule(
            and_(
                normalized('неизвестная'),
                not_(eq_lower('неизвестно')),
                not_(eq_lower('неизвестном')),
                not_(eq_lower('неизвестное')),
            ),
        ).interpretation(who.position),
        rule(
            and_(
                normalized('неизвестная'),
                not_(eq_lower('неизвестно')),
                not_(eq_lower('неизвестном')),
                not_(eq_lower('неизвестное')),
            ),
            dictionary(['мужчина', 'женщина', 'группа', 'человек']),
        ).interpretation(who.position),
        rule(
            eq_lower('н'),
            or_(
                eq('-'),
                eq('/')
            ),
            or_(
                eq_lower('л'),
                eq_lower('у'),
                normalized('лицо'),
            ),
            normalized('лицо').optional(),
        ).interpretation(who.position.inflected()),
        rule(
            or_(
                normalized('неизвестное'),
                normalized('неустановленное'),
            ),
            or_(
                normalized('лицо'),
                normalized('лица'),
            ),
        ).interpretation(who.position.inflected()),
    ),
)
__ACTION = or_(
    rule(
        dictionary(['ответственность', 'вести', 'привлечь', 'смерть', 'скончаться', 'умереть', 'избить', 'избиение', 'бить']),
    ),
    rule(
        normalized('т'),
        SEP,
        normalized('повреждение'),
    ),
    rule(
        normalized('телесный'),
        normalized('повреждение'),
    ),
)
__NAME = or_(
    rule(
        DATE_NAME_RULE.interpretation(who.person),
        SEP.optional(),
        ADDRESS_RULE.optional().interpretation(who.address),
    ),
    rule(
        ADDRESS_RULE.interpretation(who.address),
        SEP.optional(),
        DATE_NAME_RULE.optional().interpretation(who.person),
    ),
)
__ACTION_PERSON = or_(
    rule(
        rule(
            or_(
                rule(
                    normalized('привлечь'),
                    gram('PREP').optional(),
                    or_(
                        normalized('уголовный'),
                        normalized('угол'),
                        normalized('уг'),
                        normalized('административный'),
                        normalized('адм'),
                        normalized('админ'),
                    ).optional(),
                    normalized('ответственность'),
                ),
                __ACTION,
            ),
        ),
        __PERSON.optional(),
    ),
    rule(
        __PERSON,
        __ACTION.optional(),
    )
)
__BODY = rule(
    or_(
        rule(
            __ACTION_PERSON,
            __NAME,
        ),
        rule(
            __NAME,
            __ACTION_PERSON,
        ),
    ),
)
__UNKNOW_PERSON_ADDRESS = rule(
    or_(
        rule(
            ADDRESS_RULE.interpretation(who.address).optional(),
            __UNKNOW_PERSON,
        ),
        rule(
            __UNKNOW_PERSON,
            ADDRESS_RULE.interpretation(who.address).optional(),
        ),
    )
)
WHO = or_(
    rule(
        or_(
            rule(
                normalized('в'),
                normalized('отношении'),
            ),
            rule(
                normalized('о'),
            ),
        ),
        or_(
            __UNKNOW_PERSON_ADDRESS,
            __NAME,
            __PERSON,
        )
    ),
    __UNKNOW_PERSON_ADDRESS,
    __BODY,
    __PERSON,
).interpretation(who)
