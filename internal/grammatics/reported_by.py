from yargy import or_, rule
from yargy.predicates import gram

from internal.interpretations import person, name
from internal.grammatics.other.address import ADDRESS_RULE
from internal.grammatics.other.birthday import BIRTHDAY
from internal.grammatics.other.utils import NAME, SEP, DATE
from internal.utils import generate_all_permutations, get_resolved_shorts, eq_lower, generate_all_subsets

__REPORTED_BY_RULE = rule(
    SEP.optional(),
    get_resolved_shorts('applicant'),
    gram('PREP').optional(),
    SEP.optional(),
)
__NAME_INTERP = NAME.interpretation(person.name)
__DATE_INTERP = DATE.interpretation(person.birthday)
__DATE_NAME_RULE = rule(
    get_resolved_shorts('citizen').optional(),
    or_(
        *([BIRTHDAY] + generate_all_subsets([__NAME_INTERP, __DATE_INTERP], SEP.optional()))
    ),
)
__RAPORT = rule(
    eq_lower('рапорт'),
    __DATE_NAME_RULE,
)
# TODO: согласно доке, interpretation(name.name).interpretation(name) должна падать, но работает валидно
__ADDRESS_RULE = ADDRESS_RULE.interpretation(person.address)
__ANONYMOUS_INTERP = rule(
    get_resolved_shorts('anonymous').interpretation(name.name.inflected())
).interpretation(name).interpretation(person.name).interpretation(person)
ANON_REPORTED_BY = rule(
    or_(
        *generate_all_permutations([__REPORTED_BY_RULE, __ANONYMOUS_INTERP], SEP.optional()),
    ),
).interpretation(person)
REPORTED_BY = rule(
    or_(
        *(
                [__RAPORT] +
                generate_all_permutations([__REPORTED_BY_RULE, __DATE_NAME_RULE], SEP.optional()) +
                generate_all_permutations([__REPORTED_BY_RULE, __ADDRESS_RULE], SEP.optional()) +
                generate_all_permutations([__REPORTED_BY_RULE, __DATE_NAME_RULE, __ADDRESS_RULE], SEP.optional())
        ),
    )
).interpretation(person)
