from itertools import permutations
from operator import itemgetter

from yargy import or_, rule
from yargy.predicates import custom, dictionary, eq, Predicate
from yargy.predicates.constructors import ParameterPredicate

from internal import templates


class LengthLte(ParameterPredicate):

    def __call__(self, token):
        return len(token.value) <= self.value


class LengthGte(ParameterPredicate):

    def __call__(self, token):
        return len(token.value) >= self.value


def get_resolved_shorts(key):
    SHORTS = rule(
        dictionary(templates['shorts'][key]),
        eq('.').optional(),
    )
    hyphen_lists = [hyphen.split('-') for hyphen in templates['hyphen'][key]]
    first, second = list(map(itemgetter(0), hyphen_lists)), list(map(itemgetter(1), hyphen_lists))
    # TODO: возможно, бага
    HYPHEN = rule(
        dictionary(first),
        eq('-'),
        dictionary(second),
        eq('.').optional(),
    ),
    FULL = rule(
        dictionary(templates['full'][key]),
    )
    return rule(or_(SHORTS, HYPHEN[0], FULL))


def generate_all_permutations(list_, sep=None, prefix=None, suffix=None):
    perm = list(permutations(list_))
    for ind, prod in enumerate(perm):
        prod_ = []
        for rule_ in prod[:-1]:
            prod_.append(rule_)
            if sep is not None:
                prod_.append(sep)
        prod_.append(prod[-1])
        perm[ind] = rule(*prod_)
        perm[ind] = perm[ind] if suffix is None else rule(perm[ind], suffix)
        perm[ind] = perm[ind] if prefix is None else rule(prefix, perm[ind])
    return list(perm)


def __is_bit_set(num, bit):
    return num & (1 << bit) > 0


def __subsets(s):
    sets = []
    for i in range(1 << len(s)):
        subset = [s[bit] for bit in range(len(s)) if __is_bit_set(i, bit)]
        sets.append(subset)
    return sets


def generate_all_subsets(list_, sep=None, prefix_rule=None, suffix_rule=None):
    all_subset = []
    for subset in __subsets(list_)[1:]:
        all_subset += generate_all_permutations(subset, sep, prefix=prefix_rule, suffix=suffix_rule)
    return all_subset


eq_lower = lambda word: custom(lambda x: x.lower() == word)
is_letter = custom(lambda x: x.isalpha())
is_numeric = custom(lambda x: x.isnumeric())
is_not_numeric = custom(lambda x: not x.isnumeric())
