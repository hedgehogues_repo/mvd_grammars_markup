from yargy.interpretation import fact, attribute

date = fact(
    'date',
    ['date'],
)
time = fact(
    'time',
    ['hours', 'minutes'],
)
interval = fact(
    'interval',
    ['from_', 'to'],
)
name = fact(
    'name',
    ['name'],
)
address = fact(
    'address',
    ['address', 'prefix'],
)
source = fact(
    'source',
    ['source'],
)
cusp = fact(
    'cusp',
    ['id'],
)
attach_to = fact(
    'attach_to',
    ['cusp'],
)
person = fact(
    'person',
    ['birthday', 'name', 'address'],
)
reported_at = fact(
    'reported_at',
    ['date', 'time', 'report'],
)
run_by_item = fact(
    'run_by_item',
    ['position', 'name'],
)
run_by = fact(
    'run_by',
    [attribute('items').repeatable(), 'time'],
)
text = fact(
    'text',
    ['text'],
)
organisation = fact(
    'organisation',
    ['name', 'type'],
)
where = fact(
    'where',
    ['organisation', 'address'],
)
who = fact(
    'who',
    ['person', 'address', 'position'],
)
when = fact(
    'when',
    ['date', 'time', 'interval', 'address'],
)
