from collections import OrderedDict

from internal.enums import ZonesKeys


class CombinerAlgorithm:
    @classmethod
    def __extract_cusp_attached_to(cls, data):
        attached_to = [fact.cusp.id for fact in data.description.zones[ZonesKeys.ATTACHED_TO]]
        cusp = [fact.id for fact in data.description.zones[ZonesKeys.CUSP]]
        set_cusp = set(cusp)
        set_attached_to = set(attached_to)
        return list(set_cusp - set_attached_to), list(set_attached_to)

    @classmethod
    def __get_source(cls, data):
        glonass_fact = data.description.zones[ZonesKeys.GLONASS]
        source_fact = data.description.zones[ZonesKeys.SOURCE]
        source = [{'source': fact.source} for fact in glonass_fact if fact.source is not None] +\
                 [fact.source for fact in source_fact if fact.source is not None]
        return source

    def __get_reported_by(self, data):
        json_ = self.__get_json(data, ZonesKeys.ANON_REPORTED_BY)
        reported_by = [] if json_ is None else json_
        json_ = self.__get_json(data, ZonesKeys.REPORTED_BY)
        reported_by += [] if json_ is None else json_
        return reported_by

    def __transform_to_dict(self, json_):
        for key in json_:
            if type(json_[key]) == OrderedDict:
                json_[key] = self.__transform_to_dict(json_[key])
            if type(json_[key]) == list:
                for ind, elem in enumerate(json_[key]):
                    json_[key][ind] = self.__transform_to_dict(elem)
        return dict(json_)

    def __get_json(self, data, key):
        return [self.__transform_to_dict(item.as_json) for item in data.description.zones[key] if len(item.as_json) > 0]

    def __get_reported_at_when(self, data):
        when = self.__get_json(data, ZonesKeys.WHEN)
        reported_at = self.__get_json(data, ZonesKeys.REPORTED_AT)
        pairs = {}

        # Если в when есть адрес, значит эту запись (если она есть в reported_at) нужно удалить, если адреса нет, то
        # нужно удалить эту запись из when (если она есть в when)
        for ind_when, fact_when in enumerate(when):
            if not ('time' in fact_when and 'date' in fact_when):
                continue
            for ind_reported_at, fact_reported_at in enumerate(reported_at):
                if not ('time' in fact_reported_at and 'date' in fact_reported_at):
                    continue
                if fact_when['time'] == fact_reported_at['time'] and fact_when['date'] == fact_reported_at['date']:
                    pairs[ind_when] = ind_reported_at
        for ind_when, ind_reported_at in pairs.items():
            if 'address' in when[ind_when]:
                reported_at = reported_at[:ind_reported_at] + reported_at[ind_reported_at+1:]
            else:
                when = when[:ind_when] + when[ind_when + 1:]
        return reported_at, when

    def get_where(self, data):
        where = self.__get_json(data, ZonesKeys.WHERE)
        address = self.__get_json(data, ZonesKeys.ADDRESS)
        reported_by = data.description.zones[ZonesKeys.RESULT_JSON]['reported_by']
        who = data.description.zones[ZonesKeys.RESULT_JSON]['who']
        reported_by_address_bool = len([True for dict_ in reported_by if 'address' in dict_]) == 0
        where_bool = len([True for dict_ in where if 'address' in dict_]) == 0
        who_bool = len([True for dict_ in who if 'address' in dict_]) == 0

        if where_bool and reported_by_address_bool and who_bool and len(where) == 0:
            return address
        else:
            return where

    def transform(self, data):
        data.description.zones[ZonesKeys.RESULT_JSON] = {}

        cusp, attached_to = self.__extract_cusp_attached_to(data)
        data.description.zones[ZonesKeys.RESULT_JSON]['id'] = cusp
        data.description.zones[ZonesKeys.RESULT_JSON]['attached_to'] = attached_to
        reported_at, when = self.__get_reported_at_when(data)
        data.description.zones[ZonesKeys.RESULT_JSON]['reported_at'] = reported_at
        data.description.zones[ZonesKeys.RESULT_JSON]['when'] = when

        data.description.zones[ZonesKeys.RESULT_JSON]['reported_by'] = self.__get_reported_by(data)
        data.description.zones[ZonesKeys.RESULT_JSON]['source'] = self.__get_source(data)

        data.description.zones[ZonesKeys.RESULT_JSON]['run_by'] = self.__get_json(data, ZonesKeys.RUN_BY)
        data.description.zones[ZonesKeys.RESULT_JSON]['text'] = self.__get_json(data, ZonesKeys.TEXT)
        data.description.zones[ZonesKeys.RESULT_JSON]['who'] = self.__get_json(data, ZonesKeys.WHO)
        data.description.zones[ZonesKeys.RESULT_JSON]['where'] = self.get_where(data)
        # print("###############")
        # print(data.data[:-1])
        # print('cusp:', data.description.zones[ZonesKeys.RESULT_JSON]['cusp'])
        # print('attached_to:', data.description.zones[ZonesKeys.RESULT_JSON]['attached to'])
        # print('run_by:', data.description.zones[ZonesKeys.RESULT_JSON]['run by'])
        # print('source:', data.description.zones[ZonesKeys.RESULT_JSON]['source'])
        # print('reported_at:', data.description.zones[ZonesKeys.RESULT_JSON]['reported at'])
        # print('when:', data.description.zones[ZonesKeys.RESULT_JSON]['when'])
        # print('reported_by:', data.description.zones[ZonesKeys.RESULT_JSON]['reported by'])
        # print('text:', data.description.zones[ZonesKeys.RESULT_JSON]['text'])
        # print('who:', data.description.zones[ZonesKeys.RESULT_JSON]['who'])
        # print('where:', data.description.zones[ZonesKeys.RESULT_JSON]['where'])
