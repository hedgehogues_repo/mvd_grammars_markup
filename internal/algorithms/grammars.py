from yargy import Parser


class GrammarAlgorithm:
    def __init__(self, grammar, key):
        self.__parser = Parser(grammar)
        self.__zone_key = key

    def transform(self, data):
        data.description.zones[self.__zone_key] = []
        for match in self.__parser.findall(data.data):
            data.description.zones[self.__zone_key].append(match.fact)
