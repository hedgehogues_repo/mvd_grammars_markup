class Description:
    def __init__(self, src=None, zones=None, dataset=None):
        self.dataset = dataset
        self.src = src
        self.zones = {} if zones is None else zones


class Data:

    def __init__(self, description_reader=None, description_writer=None, data_reader=None, data_writer=None):
        self.data_reader = data_reader
        self.data_writer = data_writer
        self.description_reader = description_reader
        self.description_writer = description_writer

        self.data = None
        self.description = None

    def close(self):
        if self.description_reader is not None:
            self.description_reader.close()
        if self.description_writer is not None:
            self.description_writer.close()
        if self.data_reader is not None:
            self.data_reader.close()
        if self.data_writer is not None:
            self.data_writer.close()

    def dump(self):
        if self.description_writer is not None:
            self.description_writer.write(self.description, self.data)
        if self.data_writer is not None:
            self.data_writer.write(self.data)

    def load(self):

        self.description = Description()
        if self.description_reader is not None:
            self.description = self.description_reader.read()
        if self.description is None:
            return False

        if self.data_reader is not None and self.description is not None:
            self.data = self.data_reader.read(path=self.description.src)
        if self.data is None:
            return False

        return True
