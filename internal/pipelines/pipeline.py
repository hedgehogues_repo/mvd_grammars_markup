import multiprocessing
from internal.pipelines.statistics import BaseStatistics


class Pipeline:

    def __init__(self, data, max_data_per_process=None, print_step=0):
        self.data = data
        self.algorithms = ()
        self.print_step = print_step
        self.max_data = [max_data_per_process for _ in self.data]

        self.statistics = BaseStatistics()
        self.__data_loader = lambda data_, statistics, process_number: data_[process_number].load()
        self.__data_dumper = lambda data_, statistics, process_number: data_[process_number].dump()

    def __exit__(self, exc_type, exc_val, exc_tb):
        for image in self.data:
            image.close()

    def __is_finish(self, process_number, step_number):
        return self.max_data[process_number] is not None and step_number >= self.max_data[process_number]

    def __process(self, process_number):
        self.statistics.init_process(self.data, process_number)

        step_number = 0
        while True:
            if self.__is_finish(process_number, step_number):
                break
            if not self.__data_loader(self.data, self.statistics, process_number):
                break

            self.statistics.init_algorithms(self.data, process_number)
            for name, algorithm in self.algorithms:
                algorithm.transform(self.data[process_number])

            self.statistics.exit_algorithms(self.data, process_number)
            self.__data_dumper(self.data, self.statistics, process_number)

            # TODO: вынести в логгер. Логгер должен быть единым для Statistics и для Pipeline
            if self.print_step > 0 and step_number % self.print_step == 0:
                if self.max_data[process_number] is not None:
                    print('Process #' + str(process_number) + '. File number ' + str(step_number + 1) +
                          ' from', self.max_data[process_number])
                else:
                    print('Process #' + str(process_number) + '. File number ' + str(step_number + 1))
            step_number += 1

        self.data[process_number].close()
        self.statistics.exit_process(self.data, process_number)

    def set_algorithms(self, algorithms):
        self.algorithms = algorithms

    def set_statistics(self, statistics):
        self.statistics = statistics

    def set_image_loader(self, loader):
        self.__data_loader = loader

    def set_image_dumper(self, dumper):
        self.__data_dumper = dumper

    def start(self):
        self.statistics.init_pipeline(self.data)

        all_processes = []
        for process_number, image in enumerate(self.data):
            process = multiprocessing.Process(target=self.__process, args=(process_number,))
            all_processes.append(process)
            process.start()
        for pr_num, p in enumerate(all_processes):
            p.join()

        self.statistics.exit_pipeline(self.data)
