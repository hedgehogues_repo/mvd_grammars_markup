class BaseStatistics:
    def __init__(self):
        self._stats = {}

    def get_stats(self):
        return self._stats

    def init_algorithms(self, images, process_number):
        return

    def exit_algorithms(self, images, process_number):
        return

    def init_process(self, images, process_number):
        return

    def exit_process(self, images, process_number):
        return

    def init_pipeline(self, images):
        return

    def exit_pipeline(self, images):
        return
