import os


class Merger:

    def __init__(self, in_paths, out_file, remove):
        self.in_paths = in_paths
        self.remove = remove
        self.out_file = open(out_file, 'w')

    def __process_file(self, cur_file):
        while True:
            line = cur_file.readline()
            if len(line) == 0:
                break
            self.out_file.write(line)

    def __remove_temp_files(self):
        pass

    def merge(self):
        for path in self.in_paths:
            cur_file = open(path)
            self.__process_file(cur_file)
            cur_file.close()
            if self.remove:
                os.remove(path)

        self.out_file.close()

