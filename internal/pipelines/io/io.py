from internal.enums import ZonesKeys


class DataFileReader:
    def __init__(self, path):
        self.fd = open(path)

    def read(self, path):
        data = self.fd.readline()
        return None if len(data) == 0 else data

    def close(self):
        self.fd.close()


class DescriptionFileWriter:
    def __init__(self, path):
        self.fd = open(path, 'w')

    @classmethod
    def when(cls, zone, key):
        if key != 'when':
            return zone
        for ind, item in enumerate(zone):
            res = {}
            if 'date' in item:
                res['year'] = item['date']['date']
                res['month'] = item['date']['date']
                res['day'] = item['date']['date']
            if 'time' in item:
                res['minute'] = item['time']['minutes']
                res['hour'] = item['time']['hours']
            if 'interval' in item:
                if 'from_' in item['interval']:
                    if 'hours' in item['interval']['from_']:
                        res['from'] = {}
                        res['from']['minute'] = item['interval']['from_']['minutes']
                        res['from']['hour'] = item['interval']['from_']['hours']
                    if 'date' in item['interval']['from_']:
                        res['from'] = {}
                        res['from']['minute'] = item['interval']['from_']['date']
                        res['from']['hour'] = item['interval']['from_']['date']
                if 'to' in item['interval']:
                    if 'hours' in item['interval']['to']:
                        res['to'] = {}
                        res['to']['minute'] = item['interval']['to']['minutes']
                        res['to']['hour'] = item['interval']['to']['hours']
                    if 'date' in item['interval']['to']:
                        res['to'] = {}
                        res['to']['minute'] = item['interval']['to']['date']
                        res['to']['hour'] = item['interval']['to']['date']
            zone[ind] = res
        if len(zone) == 1:
            zone = zone[0]
        return zone

    @classmethod
    def attached_to(cls, zone, key):
        if key != 'attached_to':
            return zone
        if len(zone) >= 1:
            zone = zone[0]
        return zone

    @classmethod
    def cusp(cls, zone, key):
        if key != 'id':
            return zone
        if len(zone) >= 1:
            zone = zone[0]
        return zone

    @classmethod
    def where(cls, zone, key):
        if key != 'where':
            return zone
        for ind, item in enumerate(zone):
            res = {}
            if 'organisation' in item:
                if 'name' in item['organisation']:
                    res['organisation'] = {}
                    res['organisation']['name'] = item['organisation']['name']['name']
                if 'type' in item['organisation']:
                    if 'organisation' in res:
                        res['organisation']['type'] = item['organisation']['type']
                    else:
                        res['organisation'] = {}
                        res['organisation']['type'] = item['organisation']['type']
            if 'address' in item:
                res['address'] = item['address']
            zone[ind] = res
        if len(zone) == 1:
            zone = zone[0]
        return zone

    @classmethod
    def text(cls, zone, key):
        if key != 'text':
            return zone
        if len(zone) >= 1:
            zone = zone[0]
        return zone

    @classmethod
    def who(cls, zone, key):
        if key != 'who':
            return zone
        for ind, item in enumerate(zone):
            res = {}
            if 'position' in item:
                res['position'] = item['position']
            if 'person' in item:
                if 'name' in item:
                    name = item['name']['name']
                    if name == 'аноним':
                        res['name'] = name
                    else:
                        res['name'] = {}
                        res['name']['first'] = name
                        res['name']['second'] = name
                        res['name']['middle'] = name
                if 'birthday' in item:
                    date = item['birthday']['date']
                    res['birth'] = {}
                    res['birth']['day'] = date
                    res['birth']['month'] = date
                    res['birth']['year'] = date
                if 'address' in item:
                    res['address'] = item['address']['address']
            zone[ind] = res
        if len(zone) == 1:
            zone = zone[0]
        return zone

    @classmethod
    def reported_by(cls, zone, key):
        if key != 'reported_by':
            return zone
        for ind, item in enumerate(zone):
            res = {}
            if 'name' in item:
                name = item['name']['name']
                if name == 'аноним':
                    res['name'] = name
                else:
                    res['name'] = {}
                    res['name']['first'] = name
                    res['name']['second'] = name
                    res['name']['middle'] = name
            if 'birthday' in item:
                date = item['birthday']['date']
                res['birth'] = {}
                res['birth']['day'] = date
                res['birth']['month'] = date
                res['birth']['year'] = date
            if 'address' in item:
                res['address'] = item['address']['address']
            zone[ind] = res
        if len(zone) == 1:
            zone = zone[0]
        return zone

    @classmethod
    def reported_at(cls, zone, key):
        if key != 'reported_at':
            return zone
        for ind, item in enumerate(zone):
            res = {}
            if 'date' in item:
                res['year'] = item['date']['date']
                res['month'] = item['date']['date']
                res['day'] = item['date']['date']
            if 'time' in item:
                res['minute'] = item['time']['minutes']
                res['hour'] = item['time']['hours']
            zone[ind] = res
        if len(zone) == 1:
            zone = zone[0]
        return zone

    def write(self, description, line):
        res_dict = {}
        for key, zone in description.zones[ZonesKeys.RESULT_JSON].items():
            zone = self.reported_at(zone, key)
            zone = self.reported_by(zone, key)
            zone = self.text(zone, key)
            zone = self.where(zone, key)
            zone = self.who(zone, key)
            zone = self.attached_to(zone, key)
            zone = self.cusp(zone, key)
            zone = self.when(zone, key)
            if len(zone) > 0:
                res_dict[key] = zone
        self.fd.write(line)
        self.fd.write(str(res_dict) + '\n')
        self.fd.write('\n')

    def close(self):
        self.fd.close()
