import os


class FileSeparator:

    def __init__(self, in_path, out_path, files_proportion, max_samples=None):
        self.__file_number = -1
        self.__out_file = None

        self.count_lines = 0
        self.samples_per_file = []

        if type(files_proportion) is list:
            assert sum(files_proportion) == 1 and \
                   len([element for element in files_proportion if element >= 0]) == len(files_proportion)
        else:
            assert type(files_proportion) is int and files_proportion >= 1
            files_proportion = [1 / files_proportion] * files_proportion

        self.files_proportion = files_proportion
        self.in_path = in_path
        self.max_samples = max_samples if max_samples is not None else None
        self.out_path = out_path

        self.__calc_count_lines()
        self.__calc_images_per_file()

    def __calc_count_lines(self):
        in_file = open(self.in_path, 'r')
        self.count_lines = 0
        while True:
            if len(in_file.readline()) == 0 or self.max_samples is not None and \
               self.count_lines >= self.max_samples:
                    break
            self.count_lines += 1
        in_file.close()

    def __calc_images_per_file(self):
        self.samples_per_file = []
        for file_proportion in self.files_proportion[:-1]:
            self.samples_per_file.append(int(self.count_lines * file_proportion))
        self.samples_per_file.append(self.count_lines - sum(self.samples_per_file))

    def separate(self):
        line_number = 0
        path_to_cur_file = [self.__next_out_fd()]
        in_file = open(self.in_path, 'r')
        count_images = 0
        while True:
            line = in_file.readline()
            if len(line) == 0 or self.max_samples is not None and count_images >= self.max_samples:
                self.__out_file.close()
                break

            if line_number == self.samples_per_file[self.__file_number]:
                line_number = 0
                path_to_cur_file.append(self.__next_out_fd())

            self.__out_file.write(line)
            line_number += 1
            count_images += 1

        in_file.close()

        return path_to_cur_file

    def __generate_out_file_name(self):
        if type(self.out_path) is str:
            return os.path.abspath(self.out_path + str(self.__file_number) + '.json')
        if type(self.out_path) is list and len(self.out_path) == len(self.files_proportion):
            return os.path.abspath(self.out_path[self.__file_number])
        assert False

    def __next_out_fd(self):
        if self.__out_file is not None:
            self.__out_file.close()
        self.__file_number += 1
        path = self.__generate_out_file_name()
        self.__out_file = open(path, 'w')
        return path
