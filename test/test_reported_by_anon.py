import unittest

from yargy import Parser

from internal.interpretations import person
from internal.grammatics.reported_by import ANON_REPORTED_BY
from test.base import BaseTest


class TestAnonAttachedTo(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'КУСП 8615  [DATE] В 01:02 ЗАЯВИТЕЛЬ АНОНИМ О [NAME], ЧТО ОКОЛО ТРАНСФОРМАТОРНОЙ БУДКИ ВО ДВОРЕ ДОМА 71 ПО [ADDRESS] НЕСКОЛЬКО ЧЕЛОВЕК В ЧЕРНОЙ [NAME] ШУМЯТ, СЛУШАЮТ ГРОМКУЮ МУЗЫКУ, ВОЗМОЖНО, УПОТРЕБЛЯЮТ ЗАПРЕЩЕННЫЕ ПРЕПАРАТЫ. ЗАНИМ.: ППС.',
            '02 ЗВ. [DATE], [ADDRESS], НЕИЗВЕСТНЫЕ ЛИЦА ПО[ADDRESS] УСТАНОВИЛИ НЕЗАКОННО ВИДЕОКАМЕРУ. ЗАНИМ.',
            'КУСП 8615  [DATE] В 01:02  АНОНИМ ЗАЯВИЛ О [NAME], ЧТО ОКОЛО ТРАНСФОРМАТОРНОЙ БУДКИ ВО ДВОРЕ ДОМА 71 ПО [ADDRESS] НЕСКОЛЬКО ЧЕЛОВЕК В ЧЕРНОЙ [NAME] ШУМЯТ, СЛУШАЮТ ГРОМКУЮ МУЗЫКУ, ВОЗМОЖНО, УПОТРЕБЛЯЮТ ЗАПРЕЩЕННЫЕ ПРЕПАРАТЫ. ЗАНИМ.: ППС.',
            'КУСП 8615  [DATE] В 01:02  АНОНИМ СООБЩИЛ О [NAME], ЧТО ОКОЛО ТРАНСФОРМАТОРНОЙ БУДКИ ВО ДВОРЕ ДОМА 71 ПО [ADDRESS] НЕСКОЛЬКО ЧЕЛОВЕК В ЧЕРНОЙ [NAME] ШУМЯТ, СЛУШАЮТ ГРОМКУЮ МУЗЫКУ, ВОЗМОЖНО, УПОТРЕБЛЯЮТ ЗАПРЕЩЕННЫЕ ПРЕПАРАТЫ. ЗАНИМ.: ППС.',
            'КУСП 6228 ОТ [DATE]. СООБ. ПОСТ. 21.00. ЗАЯВМИТЕЛЬ: АНОНИМ О [NAME], ЧТО НА [ADDRESS] НА ДЕТСКОЙ ПЛОЩАДКЕ СОТРУДНИК ПОЛИЦИИ В ФОРМЕ ВМЕСТЕ С ЖЕНОЙ И РЕБЕНКОМ РАСПИВАЕТ СПИРТНЫЕ НАПИТКИ. ЗАНИМ. ПА 1513',
            'КУСП 6228 ОТ [DATE]. СООБ. ПОСТ. 21.00. ЗАЯвИТЕЛЬ: аноним СООБЩИЛ НА ТЕЛЕФОН ДОВЕРИЯ О [NAME], ЧТО НА [ADDRESS] НА ДЕТСКОЙ ПЛОЩАДКЕ СОТРУДНИК ПОЛИЦИИ В ФОРМЕ ВМЕСТЕ С ЖЕНОЙ И РЕБЕНКОМ РАСПИВАЕТ СПИРТНЫЕ НАПИТКИ. ЗАНИМ. ПА 1513',
            'КУСП 6228 ОТ [DATE]. СООБ. ПОСТ. 21.00. ЗАЯвИТЕЛЬ: анон СООБЩИЛ НА ТЕЛЕФОН ДОВЕРИЯ О [NAME], ЧТО НА [ADDRESS] НА ДЕТСКОЙ ПЛОЩАДКЕ СОТРУДНИК ПОЛИЦИИ В ФОРМЕ ВМЕСТЕ С ЖЕНОЙ И РЕБЕНКОМ РАСПИВАЕТ СПИРТНЫЕ НАПИТКИ. ЗАНИМ. ПА 1513',

        ]
        self.answers = [
            ['ЗАЯВИТЕЛЬАНОНИМ'],
            [],
            ['АНОНИМЗАЯВИЛО'],  # TODO: лишний предлог
            ['АНОНИМСООБЩИЛО'],  # TODO: лишний предлог
            [],  # TODO: спелчекер 'ЗАЯВМИТЕЛЬ:АНОНИМ'
            ['.ЗАЯвИТЕЛЬ:аноним'],
            ['.ЗАЯвИТЕЛЬ:анон'],
        ]
        self.facts = [
            ['аноним'],
            [],
            ['аноним'],  # TODO: лишний предлог
            ['аноним'],  # TODO: лишний предлог
            [],  # TODO: спелчекер 'ЗАЯВМИТЕЛЬ:АНОНИМ'
            ['аноним'],
            ['анон'],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        date_bool = fact.birthday is None
        address_bool = fact.address is None
        name_bool = fact.name.name == target_fact
        is_valid_type = type(fact) == person
        return date_bool and name_bool and address_bool and is_valid_type

    def test(self):
        self.base.base_test(Parser(ANON_REPORTED_BY), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
