import unittest

from yargy import Parser

from internal.interpretations import source
from internal.grammatics.source import SOURCE
from test.base import BaseTest


class TestSource(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            '\nСООБЩЕНИЕ ПО ТЕЛЕФОНУ "02". ЗАЯВИТЕЛЬ [NAME], [DATE].Р., [ADDRESS].ПРОСИТ УЧАСТКОВОГО.\n',
            '\n[DATE]. в 21:02 ч. обратился [NAME][DATE],, [ADDRESS], что семейный скандал с братом.\n',
            '\n02 [NAME] 19 А-98 [DATE] ОКОЛО МАГ. "[NAME]" ПРИСТАЕТ ПЬЯНЫЙ МУЖЧИНА.  ВЫЕЗД ПА-312\n',
            '\nСообщение от оператора "02":[ADDRESS], в посадке, сильно шумят. кричат, громкая музыка, катаются на а/м\n',
            '\nСообщение от оператора "Глонасс-112":[ADDRESS], в посадке, сильно шумят. кричат, громкая музыка, катаются на а/м\n',
            '\nСообщение от оператора "03":[ADDRESS], в посадке, сильно шумят. кричат, громкая музыка, катаются на а/м\n',
            '\nСообщение от оператора "04":[ADDRESS], в посадке, сильно шумят. кричат, громкая музыка, катаются на а/м\n',
            '\nСообщение от оператора "112":[ADDRESS], в посадке, сильно шумят. кричат, громкая музыка, катаются на а/м\n',
        ]
        self.answers = [
            ['"02"'],
            ['21:02ч.'],
            ['02'],
            ['"02"'],
            ['112'],
            ['"03"'],
            ['"04"'],
            ['"112"'],
        ]
        self.fact = [
            ['02'],
            [None],
            ['02'],
            ['02'],
            ['112'],
            ['03'],
            ['04'],
            ['112'],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == source
        return fact.source == target_fact and is_valid_type

    def test(self):
        self.base.base_test(Parser(SOURCE), self.tests, self.answers, self.fact, TestSource.fact_assertion)


if __name__ == '__main__':
    unittest.main()
