import unittest

from yargy import Parser

from internal.grammatics.who import WHO
from internal.interpretations import who
from test.base import BaseTest


class TestWho(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'просит привлечь к уголовной ответственности [NAME] который ударил его в лицо при этом разбил очки и причинил телесные повреждения',
            'о том, что осужденный [NAME] . из ИК-6 УФСИН по мобильной связи угрожает убийством гр-ке [NAME]',
            'напротив его дома шумит молодежь на улице',
            '[DATE]. поступило сообщение [NAME], [DATE].р., [ADDRESS] о том, что в угловом магазине за "Центром занятости" по [ADDRESS] продают фанфурики и собирается молодежь.Магазин находится возле магазина "Иволга".Выезжал в 10:40 ч. УУП [NAME]',
            '02 З-ЛЬ [[NAME] [ADDRESS] НА ТЕРРИТОРИИ 69 ШКОЛЫ СТОИТ МОЛОДЕЖЬ РАСПИВАЮТ СПИРТНЫЕ НАПИТКИ, РЯДОМ 2 СОБАКИ ТРАВЯТ НА ПРОХОЖИХ.',
            'ЗАЯВИТЕЛЬ ЖЕНА [NAME]  СОСЕД ИЗБИЛ МУЖА ЗАЯВИТЕЛЯ',
            'КУСП 4671 [DATE] В 09:50 ЗАЯВИТЕЛЬ ГР. [NAME], [DATE].Р., [ADDRESS]  О [NAME], ЧТО СОСЕДИ ЗАКРЫЛИ ОБЩУЮ ТАМБУРНУЮ ДВЕРЬ, ЗАЯВИТЕЛЬНИЦА НЕ МОЖЕТ ВЫЙТИ НА УЛИЦУ. ЗАНИМ.: УУП [NAME].',
            'у соседеи заявителя происходит скандал',
            '[NAME][DATE].Р.[ADDRESS] СОСЕД ИЗ КВ.250 ПОСТОЯННО ХУЛИГАНИТ.',
            '[DATE]. в 14:00 ушел из дома и до настоящего времени не вернулся  несовершеннолетний сын [NAME] [DATE].р. [ADDRESS]',
            '[DATE]. поступил рапорт пдн [NAME] о том, что [DATE]. несовершеннолетний [NAME][DATE].р., нанес побои несовершеннолетнему [NAME][DATE].р., [ADDRESS]',
            'Сообщение [NAME], [DATE].р, прож.[ADDRESS]   о том, что [DATE] по  месту жительства в присутствии родственников  скончался  [NAME], [DATE].р.,  [ADDRESS]. Труп  без ВПНС.',
            '02 [ADDRESS] [DATE], ОТЧИМ В А/О БУЯНИТ. ВЫЕЗД ПА-277.',
            'Сообщение по тел.З-ль [NAME], [DATE].р., [ADDRESS]  подъезде лежит мужчина, хорошо одетый.',
            'По территории СОШ №7 гоняют двое пьяных на мотоциклах',
            'По территории СОШ №7 гоняют несколько пьяных на мотоциклах',
            '[NAME] [ADDRESS] , ударила сожительница',
            '[ADDRESS], З-ЛЬ [NAME] (ДОРОЖ. РАБ) НЕАДЕКВАТНЫЙ МОЛОДОЙ ЧЕЛОВЕК ХОДИТ С НОЖОМ И МОЛОТКОМ, МЕШАЕТ РАБОТАТЬ. НЕ ОБНАРУЖЕНО.',
            'сообщила, что в ресторане двое молодых человек ведут себя вызывающе, сломали огнетушитель залив зал, оскорбляют и ломают мебель.',
            '[DATE] В 04:41 СООБЩЕНИЕ УВД О [NAME], ЧТО ПО [ADDRESS] У АВТОВОКЗАЛА НЕСКОЛЬКО ЧЕЛОВЕК С БИТАМИ ИЗБИВАЮТ ТАКСИСТА.ЗАНИМ.: ПА',
            'неизвестная женщина, что в двух этажном доме [ADDRESS] неизвестные и что то смотрят.',
            'о том, что н/л [NAME] получил т/п на автомойке по [ADDRESS]',
            'ЗАЯВЛЕНИЕ [NAME], [DATE]г.рожд.,  [ADDRESS]  В ОТНОШЕНИИ Н/ЛИЦА, КОТОРОЕ[DATE]  В 18.55 НА СТ. МЕТРО [NAME], ВЫРАЖАЛСЯ НЕЦЕНЗУРНОЙ БРАНЬЮ, ОСКОРБЛЯЛ.',
            'неизвестно неизвестном неизвестное',
            '[DATE] В 20:28 МАТЕРИАЛ ПО ФАКТУ МОШЕННИЧЕСТВА ООО [NAME] В ОТНОШЕНИИ ГР. [NAME]ЗАНИМ. ГСУ [NAME]',
            '[DATE] В 20:28 МАТЕРИАЛ ПО ФАКТУ МОШЕННИЧЕСТВА ООО [NAME] В ОТНОШЕНИИ ГР. [NAME], [DATE].р.,  [ADDRESS] ЗАНИМ. ГСУ [NAME]',
            'С [DATE] ПО [DATE]. ИЗ САДА В Д.[NAME] НЕИЗВЕСТНОЕ ЛИЦО ПОХИТИЛО КУРИЦУ',
            '[ADDRESS]"а", пос. [ADDRESS], магазин "[NAME]", мужчина ведет себя неадекватно, хулиганит, ругается нецензурными словами. Заявление [NAME], [DATE].р., [ADDRESS], раб. бар 24 часа, бармен, просит привлечь к  [NAME]/лицо которое [DATE]. находясь в помещении бара " [NAME]", расп. [ADDRESS]"а", в состоянии алкогольного опьянения вел себя грубо, приставал к заявителю, выражался нецензурной бранью.',
            '[DATE]. в 10.05ч по [ADDRESS] родственники сняли с петли гр.[NAME]. Пытался повеситься. Который до приезда скорой помощи скончался.',
            '[DATE]. в 00.51 ч. поступило сообщение "02",  [NAME] [DATE].[ADDRESS], о том, что неизвестный присылает смс сообщения с угрозами.',
            'неизвестный мужчина избивает зятя',
            'По факту обнаружения трупа [NAME] в кв.  [ADDRESS]',
            'о том, что н/у лицо [NAME] получил т/п на автомойке по [ADDRESS]',
        ]
        self.answers = [
            ['привлечькуголовнойответственности[NAME]'],
            ['осужденный[NAME].'],
            ['молодежь'],
            ['молодежь'],
            ['МОЛОДЕЖЬ'],
            ['[NAME]СОСЕДИЗБИЛ', 'ЖЕНА', 'МУЖА'],  # TODO: жена -- лишнее, муж -- лишнее (про мужа распознать сложно)
            ['О[NAME],', 'СОСЕДИ'],
            [],  # TODO: spellchecker
            ['[NAME][DATE].Р.[ADDRESS]СОСЕД'],  # TODO: непонятно, кто есть [NAME][DATE].Р.[ADDRESS]. Это сосед или заявитель
            ['несовершеннолетнийсын[NAME][DATE].р.[ADDRESS]'],
            ['несовершеннолетнему[NAME][DATE].р.,[ADDRESS]', 'несовершеннолетний[NAME][DATE].р.,'],
            ['скончался[NAME],[DATE].р.,[ADDRESS]', 'Труп'],  # TODO: Труп -- лишнее
            ['ОТЧИМ'],
            ['мужчина'],
            ['двоепьяных'],
            ['несколькопьяных'],
            ['сожительница'],
            ['МОЛОДОЙЧЕЛОВЕК'],
            ['двоемолодыхчеловек'],
            ['О[NAME],', 'НЕСКОЛЬКОЧЕЛОВЕК'],  # TODO: не хватает слова "Автовокзал" для того, чтобы распознался адрес
            ['доме[ADDRESS]неизвестные', 'неизвестнаяженщина'],  # TODO: неизвестнаяженщина -- лишнее (скорее всего она -- заявитель)
            ['н/л'],  # TODO: странно, что после н/л идёт [NAME]
            ['ВОТНОШЕНИИН/ЛИЦА'],
            [],  # Некоторые наречия не могут встречаться, т.к. фигурируют в других контекстах
            ['ВОТНОШЕНИИГР.[NAME]'],
            ['ВОТНОШЕНИИГР.[NAME],[DATE].р.,[ADDRESS]'],
            ['НЕИЗВЕСТНОЕЛИЦО'],
            ['мужчина'],  # TODO: не хватает [NAME]/лицо
            [],  # TODO: ошибка. Задетектить сложно
            ['неизвестный'],
            ['неизвестныймужчина', 'зятя'],  # TODO: ошибка. 'зятя' -- лишнее
            ['трупа[NAME]вкв.[ADDRESS]'],
            ['н/улицо'],
        ]
        self.facts = [
            [{'address': None, 'position': None, 'person': {'name': '[NAME]', 'birthday': None}}],
            [{'address': None, 'position': 'осуждённый', 'person': {'name': '[NAME]', 'birthday': None}}],
            [{'address': None, 'position': 'молодёжь', 'person': None}],
            [{'address': None, 'position': 'молодёжь', 'person': None}],
            [{'address': None, 'position': 'молодёжь', 'person': None}],
            [
                {'address': None, 'position': 'сосед', 'person': {'name': '[NAME]', 'birthday': None}},
                {'address': None, 'position': 'жена', 'person': None},
                {'address': None, 'position': 'муж', 'person': None},
            ],  # TODO: жена -- лишнее, муж -- лишнее (про мужа распознать сложно)
            [
                {'address': None, 'position': None, 'person': {'name': '[NAME]', 'birthday': None}},
                {'address': None, 'position': 'сосед', 'person': None},
            ],
            [],  # TODO: spellchecker
            [{'address': {'prefix': None, 'address': '[ADDRESS]'}, 'position': 'сосед', 'person': {'name': '[NAME]', 'birthday': '[DATE]'}}],  # TODO: непонятно, кто есть [NAME][DATE].Р.[ADDRESS]. Это сосед или заявитель
            [{'address': {'prefix': None, 'address': '[ADDRESS]'}, 'position': 'несовершеннолетний сын', 'person': {'name': '[NAME]', 'birthday': '[DATE]'}}],
            [
                {'address': {'prefix': None, 'address': '[ADDRESS]'}, 'position': 'несовершеннолетний', 'person': {'name': '[NAME]', 'birthday': '[DATE]'}},
                {'address': None, 'position': 'несовершеннолетний', 'person': {'name': '[NAME]', 'birthday': '[DATE]'}},
            ],
            [
                {'address': {'prefix': None, 'address': '[ADDRESS]'}, 'position': None, 'person': {'name': '[NAME]', 'birthday': '[DATE]'}},
                {'address': None, 'position': 'труп', 'person': None},
            ],  # TODO: Труп -- лишнее (возможно)
            [{'address': None, 'position': 'отчим', 'person': None}],
            [{'address': None, 'position': 'мужчина', 'person': None}],
            [{'address': None, 'position': 'двое пьяный', 'person': None}],  # TODO: неверный род
            [{'address': None, 'position': 'несколько пьяный', 'person': None}],  # TODO: неверный род
            [{'address': None, 'position': 'сожительница', 'person': None}],
            [{'address': None, 'position': 'молодая человек', 'person': None}],
            [{'address': None, 'position': 'двое молодая человек', 'person': None}],  # TODO: неверный род
            [
                {'address': None, 'position': None, 'person': {'name': '[NAME]', 'birthday': None}},
                {'address': None, 'position': 'несколько человек', 'person': None},
            ],  # TODO: не хватает слова "Автовокзал" для того, чтобы распознался адрес
            [
                {'address': {'prefix': 'доме', 'address': '[ADDRESS]'}, 'position': 'неизвестные', 'person': None},
                {'address': None, 'position': 'неизвестная женщина', 'person': None},
            ],  # TODO: неизвестнаяженщина -- лишнее (скорее всего она -- заявитель)
            [{'address': None, 'position': 'н/л', 'person': None}],
            [{'address': None, 'position': 'н/лицо', 'person': None}],
            [],  # Некоторые наречия не могут встречаться, т.к. фигурируют в других контекстах
            [{'address': None, 'position': None, 'person': {'name': '[NAME]', 'birthday': None}}],
            [{'address': {'prefix': None, 'address': '[ADDRESS]'}, 'position': None, 'person': {'name': '[NAME]', 'birthday': '[DATE]'}}],
            [{'address': None, 'position': 'неизвестное лицо', 'person': None}],
            [{'address': None, 'position': 'мужчина', 'person': None}],
            [],  # TODO: ошибка. Задетектить сложно
            [{'address': None, 'position': 'неизвестный', 'person': None}],
            [
                {'address': None, 'position': 'неизвестный мужчина', 'person': None},
                {'address': None, 'position': 'зять', 'person': None},
            ],  # TODO: ошибка. 'зятя' -- лишнее
            [{'address': {'prefix': 'кв.', 'address': '[ADDRESS]'}, 'position': 'труп', 'person': {'name': '[NAME]', 'birthday': None}}],
            [{'address': None, 'position': 'н/у лицо', 'person': None}],
        ]

    def fact_assertion(self, fact, target_fact):
        is_address = fact.address is None and target_fact['address'] is None or \
            fact.address.address is not None and fact.address.address == target_fact['address']['address'] and \
            fact.address.prefix == target_fact['address']['prefix']
        is_position = fact.position is None and target_fact['position'] is None or \
            fact.position is not None and target_fact['position'] == fact.position
        is_birthday = fact.person is None and target_fact['person'] is None or \
            fact.person is not None and (
                fact.person.birthday is not None and target_fact['person']['birthday'] == fact.person.birthday.date or
                fact.person.birthday is None and target_fact['person']['birthday'] is None
            )
        is_name = fact.person is None and target_fact['person'] is None or \
            fact.person is not None and (
                fact.person.name is not None and target_fact['person']['name'] == fact.person.name.name or
                fact.person.name is None and target_fact['person']['name'] is None
            )
        is_valid_type = type(fact) == who
        return is_address and is_position and is_birthday and is_name and is_valid_type

    def test(self):
        self.base.base_test(Parser(WHO), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
