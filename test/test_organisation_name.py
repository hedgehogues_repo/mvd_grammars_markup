import unittest

from yargy import Parser

from internal.grammatics.where import ORGANISATION_NAME
from internal.interpretations import organisation
from test.base import BaseTest


class TestWhereOrganisation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'КУСП 8615  [DATE] В 01:02 ЗАЯВИТЕЛЬ АНОНИМ О [NAME], ЧТО ОКОЛО ТРАНСФОРМАТОРНОЙ БУДКИ ВО ДВОРЕ ДОМА 71 ПО [ADDRESS] НЕСКОЛЬКО ЧЕЛОВЕК В ЧЕРНОЙ [NAME] ШУМЯТ, СЛУШАЮТ ГРОМКУЮ МУЗЫКУ, ВОЗМОЖНО, УПОТРЕБЛЯЮТ ЗАПРЕЩЕННЫЕ ПРЕПАРАТЫ. ЗАНИМ.: ППС.',
            '[DATE] ПОСТУПИЛО ЗАЯВЛЕНИЕ [NAME] [DATE].Р. [ADDRESS]: С. [ADDRESS] . [DATE] ЗАЯВИТЕЛЬ НАХОДЯСЬ НА УЛИЦЕ УТЕРЯЛА СОТ. ТЕЛЕФОН "[NAME]" IMEI:[NAME]. РАБОТАЛИ:[NAME]. МАТЕРИАЛ.',
            '[DATE] 20:05 сообщение [DATE].р. [ADDRESS]о том,что в театре "[NAME]"  [ADDRESS] а/м на тротуаре.',
            '[DATE] [ADDRESS]МЕЖДУ ЖИЛЫМ ДОМОМ И ТЦ "[NAME]" ЛЕЖИТ МУЖЧИНА, [DATE], ТРЯСЕТ ВОЗМОЖНО ПРИСТУП. МУЖЧИНА ОСМОТРЕН БИТ 31 (ВР. [NAME]), ОТПРАВЛЕН В ГКБ 12.',
            'ВХОД № 6712 ОТ [DATE].ПОСТУПИЛ ОТ НАЧАЛЬНИКА ОП №2 "[NAME]" [NAME] МАТЕРИАЛ ПРОВЕРКИ ПО ФАКТУ МОШЕННИЧЕСКИХ ДЕЙСТВИЙ СО СТОРОНЫ [NAME]. В ОТНОШЕНИИ [NAME]',
            'ЗВ. ТОВАРОВЕД [NAME]. КОРАБЕЛЬНАЯ 8А МАГ. "[NAME]" КРАЖА В МАГАЗИНЕ, СУММУ УЩЕРБА НАЗВАТЬ НЕ МОГУТ. ЗАДЕРЖАН МУЖЧИНА.',
            '-02- [NAME], [DATE]г.рожд [ADDRESS] АВТОБУСНАЯ ОСТАНОВКА "[NAME]" ДТП С УЧАСТИЕМ ОБЩЕСТВЕННОГО ТРАНСПОРТА. ПОСТРАДАВШИХ НЕТ',
            'СООБЩЕНИЕ ОТ [NAME]У МАГАЗИНА [NAME][ADDRESS] ЛЕЖИТ МУЖЧИНА.',
            '.[DATE] 14:55 сообщение [DATE].р. [ADDRESS] о том,что утерял сотовый телефон',
            '[DATE] 11:00 заявление [DATE].р. [ADDRESS] привлечь к н/л,которое [DATE] в ТЦ Лента вынес товар миимо кассы не оплатив.',
            '02. ЗА-ЛЬ [NAME], [DATE].Р., [ADDRESS] , МАГАЗИН "[NAME]". В МАГАЗИНЕ РАЗЬИЛАСЬ БУТЫЛКА ВОДКИ. СОТРУДНИКИ МАГАЗИНА ПРОСЯТ ЕГО ОПЛАТИТЬ. ЗА-ЛЬ ВИНОВНЫМ СЕБЯ НЕ СЧИТАЕТ. ПРОСИТ, ЧТОБЫ ПРИЕХАЛА СОГ.',
            'ВХ. СУ/326. ПО ФАКТУ МОШЕННИЧЕСТВА РУКОВОДСТВОМ КПКГ "[NAME]"',
            'ВХ. СУ/326. ПО ФАКТУ МОШЕННИЧЕСТВА РУКОВОДСТВОМ КПКГ [NAME]',
            'по факту смерти новорожденного ребенка в БУЗ ОО "[NAME]"',
            'в р-не магазина канцтовары [ADDRESS] избивает мужчину',
            'материал по заявлению гр. [NAME] по факту совершения в отношении него мошеннических действий. ([DATE]. гр. [NAME], находясь в сети интернет оформил заказ на покупку сот.тел. "айфон 6". заяв., находясь в отделении "сбербанка" расп. по [ADDRESS] осуществил перевод ден.средств. товар не предоставлен)',
            '[ADDRESS], БАР "[NAME]", сидит пьяный мужчина на поребрике. ЗАЯВИТЕЛЬ НЕ ПРЕДСТАВИЛСЯ.'
        ]
        # TODO: Захватывает лишние точки
        self.answers = [
            [],
            ['"[NAME]"'],
            ['театре"[NAME]"'],
            ['ТЦ"[NAME]"'],  # TODO: '[ADDRESS]МЕЖДУ ЖИЛЫМ ДОМОМ И ТЦ "[NAME]"'
            ['"[NAME]"'],
            ['МАГ."[NAME]"', 'МАГАЗИНЕ'],
            ['ОСТАНОВКА"[NAME]"'],
            ['МАГАЗИНА[NAME]'],
            [],
            ['ТЦ'],
            ['МАГАЗИН"[NAME]"', 'МАГАЗИНЕ', 'МАГАЗИНА'],
            ['КПКГ"[NAME]"'],
            ['КПКГ[NAME]'],
            ['ОО"[NAME]"'],
            ['магазина'],
            ['отделении"сбербанка"'],
            ['БАР"[NAME]"'],
        ]
        # TODO: Захватывает лишние точки
        self.facts = [
            [],
            [{"type": None, "name": '[NAME]'}],
            [{"type": 'театр', "name": '[NAME]'}],
            [{"type": 'тц', "name": '[NAME]'}],  # TODO: '[ADDRESS]МЕЖДУ ЖИЛЫМ ДОМОМ И ТЦ "[NAME]"'
            [{"type": None, "name": '[NAME]'}],
            [{"type": 'маг', "name": '[NAME]'}, {"type": 'магазин', "name": None}],
            [{"type": 'остановка', "name": '[NAME]'}],
            [{"type": 'магазин', "name": '[NAME]'}],
            [],
            [{"type": 'тц', "name": None}],
            [
                {"type": 'магазин', "name": '[NAME]'},
                {"type": 'магазин', "name": None},
                {"type": 'магазин', "name": None},
            ],
            [{"type": 'кпкг', "name": '[NAME]'}],
            [{"type": 'кпкг', "name": '[NAME]'}],
            [{"type": 'оо', "name": '[NAME]'}],
            [{"type": 'магазин', "name": None}],
            [{"type": 'отделение', "name": 'сбербанк'}],
            [{"type": 'бар', "name": '[NAME]'}],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == organisation
        type_bool = fact.type == target_fact['type']
        name_bool = fact.name is None or fact.name is not None and fact.name.name == target_fact['name']
        return type_bool and name_bool and is_valid_type

    def test(self):
        self.base.base_test(Parser(ORGANISATION_NAME), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
