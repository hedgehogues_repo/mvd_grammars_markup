import unittest

from yargy import Parser

from internal.grammatics.where import WHERE
from internal.interpretations import where
from test.base import BaseTest


class TestWhereAddress(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'КУСП 8615  [DATE] В 01:02 ЗАЯВИТЕЛЬ АНОНИМ О [NAME], ЧТО ОКОЛО ТРАНСФОРМАТОРНОЙ БУДКИ ВО ДВОРЕ ДОМА 71 ПО [ADDRESS] НЕСКОЛЬКО ЧЕЛОВЕК В ЧЕРНОЙ [NAME] ШУМЯТ, СЛУШАЮТ ГРОМКУЮ МУЗЫКУ, ВОЗМОЖНО, УПОТРЕБЛЯЮТ ЗАПРЕЩЕННЫЕ ПРЕПАРАТЫ. ЗАНИМ.: ППС.',
            '[DATE] ПОСТУПИЛО ЗАЯВЛЕНИЕ [NAME] [DATE].Р. [ADDRESS]: С. [ADDRESS] . [DATE] ЗАЯВИТЕЛЬ НАХОДЯСЬ НА УЛИЦЕ УТЕРЯЛА СОТ. ТЕЛЕФОН "[NAME]" IMEI:[NAME]. РАБОТАЛИ:[NAME]. МАТЕРИАЛ.',
            '[DATE] 20:05 сообщение [DATE].р. [ADDRESS]о том,что в театре "[NAME]"  [ADDRESS] а/м на тротуаре.',
            '[DATE] [ADDRESS]МЕЖДУ ЖИЛЫМ ДОМОМ И ТЦ "[NAME]" ЛЕЖИТ МУЖЧИНА, [DATE], ТРЯСЕТ ВОЗМОЖНО ПРИСТУП. МУЖЧИНА ОСМОТРЕН БИТ 31 (ВР. [NAME]), ОТПРАВЛЕН В ГКБ 12.',
            'ВХОД № 6712 ОТ [DATE].ПОСТУПИЛ ОТ НАЧАЛЬНИКА ОП №2 "[NAME]" [NAME] МАТЕРИАЛ ПРОВЕРКИ ПО ФАКТУ МОШЕННИЧЕСКИХ ДЕЙСТВИЙ СО СТОРОНЫ [NAME]. В ОТНОШЕНИИ [NAME]',
            'ЗВ. ТОВАРОВЕД [NAME]. КОРАБЕЛЬНАЯ 8А МАГ. "[NAME]" КРАЖА В МАГАЗИНЕ, СУММУ УЩЕРБА НАЗВАТЬ НЕ МОГУТ. ЗАДЕРЖАН МУЖЧИНА.',
            '-02- [NAME], [DATE]г.рожд [ADDRESS] АВТОБУСНАЯ ОСТАНОВКА "[NAME]" ДТП С УЧАСТИЕМ ОБЩЕСТВЕННОГО ТРАНСПОРТА. ПОСТРАДАВШИХ НЕТ',
            'СООБЩЕНИЕ ОТ [NAME]У МАГАЗИНА [NAME][ADDRESS] ЛЕЖИТ МУЖЧИНА.',
            '.[DATE] 14:55 сообщение [DATE].р. [ADDRESS] о том,что утерял сотовый телефон',
            '[DATE] 11:00 заявление [DATE].р. [ADDRESS] привлечь к н/л,которое [DATE] в ТЦ Лента вынес товар миимо кассы не оплатив.',
            '02. ЗА-ЛЬ [NAME], [DATE].Р., [ADDRESS] , МАГАЗИН "[NAME]". В МАГАЗИНЕ РАЗЬИЛАСЬ БУТЫЛКА ВОДКИ. СОТРУДНИКИ МАГАЗИНА ПРОСЯТ ЕГО ОПЛАТИТЬ. ЗА-ЛЬ ВИНОВНЫМ СЕБЯ НЕ СЧИТАЕТ. ПРОСИТ, ЧТОБЫ ПРИЕХАЛА СОГ.',
            'ВХ. СУ/326. ПО ФАКТУ МОШЕННИЧЕСТВА РУКОВОДСТВОМ КПКГ "[NAME]"',
            'ВХ. СУ/326. ПО ФАКТУ МОШЕННИЧЕСТВА РУКОВОДСТВОМ КПКГ [NAME]',
            'по факту смерти новорожденного ребенка в БУЗ ОО "[NAME]"',
            'в р-не магазина канцтовары [ADDRESS] избивает мужчину',
            'материал по заявлению гр. [NAME] по факту совершения в отношении него мошеннических действий. ([DATE]. гр. [NAME], находясь в сети интернет оформил заказ на покупку сот.тел. "айфон 6". заяв., находясь в отделении "сбербанка" расп. по [ADDRESS] осуществил перевод ден.средств. товар не предоставлен)',
            '[ADDRESS], БАР "[NAME]", сидит пьяный мужчина на поребрике. ЗАЯВИТЕЛЬ НЕ ПРЕДСТАВИЛСЯ.'
        ]
        # TODO: Захватывает лишние точки
        self.answers = [
            ['ПО[ADDRESS]'],
            ['"[NAME]"'],
            ['театре"[NAME]"[ADDRESS]'],
            ['ТЦ"[NAME]"'],  # TODO: '[ADDRESS]МЕЖДУ ЖИЛЫМ ДОМОМ И ТЦ "[NAME]"'
            ['"[NAME]"'],  # TODO: ОП №2 "[NAME]" [NAME]
            ['МАГ."[NAME]"', 'МАГАЗИНЕ'],
            ['[ADDRESS]АВТОБУСНАЯОСТАНОВКА"[NAME]"'],
            ['МАГАЗИНА[NAME][ADDRESS]'],
            [],
            ['ТЦ'],
            ['[ADDRESS],МАГАЗИН"[NAME]"', 'МАГАЗИНЕ', 'МАГАЗИНА'],
            ['КПКГ"[NAME]"'],
            ['КПКГ[NAME]'],
            ['ОО"[NAME]"'],
            ['магазинаканцтовары[ADDRESS]'],  # TODO: 'магазина канцтовары [ADDRESS]'
            ['отделении"сбербанка"расп.по[ADDRESS]'],
            ['[ADDRESS],БАР"[NAME]"'],
        ]
        # TODO: Захватывает лишние точки
        self.facts = [
            [{"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": None}],
            [{"address": None, "organisation": {"type": None, "name": '[NAME]'}}],
            [{"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": {"type": 'театр', "name": '[NAME]'}}],
            [{"address": None, "organisation": {"type": 'тц', "name": '[NAME]'}}],  # TODO: '[ADDRESS]МЕЖДУ ЖИЛЫМ ДОМОМ И ТЦ "[NAME]"'
            [{"address": None, "organisation": {"type": None, "name": '[NAME]'}}],
            [
                {"address": None, "organisation": {"type": 'маг', "name": '[NAME]'}},
                {"address": None, "organisation": {"type": 'магазин', "name": None}},  # TODO: лишнее
            ],
            [{"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": {"type": 'остановка', "name": '[NAME]'}}],
            [{"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": {"type": 'магазин', "name": '[NAME]'}}],
            [],
            [{"address": None, "organisation": {"type": 'тц', "name": None}}],
            [
                {"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": {"type": 'магазин', "name": '[NAME]'}},
                {"address": None, "organisation": {"type": 'магазин', "name": None}},  # TODO: лишнее
                {"address": None, "organisation": {"type": 'магазин', "name": None}},  # TODO: лишнее
            ],
            [{"address": None, "organisation": {"type": 'кпкг', "name": '[NAME]'}}],
            [{"address": None, "organisation": {"type": 'кпкг', "name": '[NAME]'}}],
            [{"address": None, "organisation": {"type": 'оо', "name": '[NAME]'}}],
            [{"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": {"type": 'магазин', "name": None}}],
            [{"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": {"type": "отделение", "name": "сбербанк"}}],
            [{"address": {'prefix': None, 'address': '[ADDRESS]'}, "organisation": {"type": 'бар', "name": '[NAME]'}}],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == where
        organisation_bool = fact.organisation is None and target_fact['organisation'] is None or \
                            fact.organisation is not None and \
                            fact.organisation.type == target_fact['organisation']['type'] and \
                            (
                                fact.organisation.name is None and target_fact['organisation']['name'] is None
                                or fact.organisation.name is not None and
                                fact.organisation.name.name == target_fact['organisation']['name']
                            )
        address_bool = fact.address is None and target_fact['address'] is None or \
            fact.address.address is not None and fact.address.address == target_fact['address']['address'] and \
            fact.address.prefix == target_fact['address']['prefix']
        return address_bool and organisation_bool and is_valid_type

    def test(self):
        self.base.base_test(Parser(WHERE), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
