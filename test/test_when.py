import unittest

from yargy import Parser

from internal.grammatics.when import WHEN
from internal.interpretations import when
from test.base import BaseTest


class TestInterval(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            '[DATE] в период с 09.00 час. до 15.00 час. неизветсные из квартиры заявительницы похитили сумку с документами и банковскими карточками.',
            'ПОЧТА ВХ. №5102. ЛИУ-1 [NAME]. МАТЕРИАЛ ПРОВЕРКИ /КУСП-29 ЛИУ-1/ ОТ [DATE]. ПО ФАКТУ ОБРАЩЕНИЯ В МЕД. ЧАСТЬ [NAME] [DATE].Р. ЗАРЕГ. В 17-02 ЧАС. [DATE]. ЗАНИМ: УУП [NAME]',
            '[DATE]. в период времени с 16ч. 00мин. до 17ч. 00мин. [NAME], [ADDRESS],  оставил свой с/т у знакомого на [ADDRESS],',
            'КУСП-1363 РАПОРТ ПОСТ. [DATE] В 17.10   [DATE] В 22.30 ПО [ADDRESS] , [NAME], [DATE].Р., [ADDRESS] , В СОСТОЯНИИ А/О, НАНЕС ПОБОИ [NAME], [DATE].Р., [ADDRESS]',
            'о том,что в период с [DATE] по [DATE] с его садового участка было похищено имущество'
        ]
        # TODO: добавь тест для ADDRESS интерпретации
        self.answers = [
            ['[DATE]впериодс09.00час.до15.00час.'],
            ['17-02ЧАС.[DATE]'],
            ['[DATE].впериодвременис16ч.00мин.до17ч.00мин.'],
            ['[DATE]В22.30ПО[ADDRESS]', '[DATE]В17.10'],  # 2ой объект -- reported_at в интерпретацию не попадет
            ['впериодс[DATE]по[DATE]'],
        ]
        self.facts = [
            [{'date': '[DATE]', 'time': None, 'interval': {'from': {'hours': 9, 'minutes': 0}, 'to':  {'hours': 15, 'minutes': 0}}}],
            [{'date': '[DATE]', 'time': {'hours': 17, 'minutes': 2}, 'interval': None}],
            [{'date': '[DATE]', 'time': None, 'interval': {'from': {'hours': 16, 'minutes': 0}, 'to':  {'hours': 17, 'minutes': 0}}}],
            [
                {'date': '[DATE]', 'time': {'hours': 22, 'minutes': 30}, 'interval': None},
                {'date': None, 'time': None, 'interval': None},
            ],
            [{'date': None, 'interval': {'from': '[DATE]', 'to':  '[DATE]'}, 'time': None}],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == when
        date_bool = target_fact['date'] is None and fact.date is None or \
                    target_fact['date'] is not None and fact.date.date == target_fact['date']
        time_bool = target_fact['time'] is None and fact.time is None or \
                    target_fact['time'] is not None and fact.time.hours == target_fact['time']['hours'] and \
                    fact.time.minutes == target_fact['time']['minutes']
        # TODO: Для интервалов работает, но писать лень
        interval_bool = True
        return date_bool and is_valid_type and time_bool and interval_bool

    def test(self):
        self.base.base_test(Parser(WHEN), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
