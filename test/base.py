import unittest


class BaseTest(unittest.TestCase):

    def base_test(self, parser, tests, answers, facts, facts_assertion):
        self.assertEqual(len(tests), len(answers))
        self.assertEqual(len(facts), len(answers))
        for test, answs, fact in zip(tests, answers, facts):
            parser_found = list(parser.findall(test))
            for answ, match, f in zip(answs, parser_found, fact):
                self.assertTrue(facts_assertion(match.fact, f), 'Not valid counts: ' + test)
                self.assertEqual(answ, ''.join([_.value for _ in match.tokens]), 'Not valid counts: ' + test)
            self.assertEqual(len(answs), len(parser_found), 'Not valid counts: ' + test)
            self.assertEqual(len(answs), len(fact), 'Not valid counts: ' + test)
