import unittest

from yargy import Parser

from internal.grammatics.run_by import RUN_BY
from internal.interpretations import run_by, run_by_item
from test.base import BaseTest


class TestAttachedTo(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'КУСП 8615  [DATE] В 01:02 ЗАЯВИТЕЛЬ АНОНИМ О [NAME], ЧТО ОКОЛО ТРАНСФОРМАТОРНОЙ БУДКИ ВО ДВОРЕ ДОМА 71 ПО [ADDRESS] НЕСКОЛЬКО ЧЕЛОВЕК В ЧЕРНОЙ [NAME] ШУМЯТ, СЛУШАЮТ ГРОМКУЮ МУЗЫКУ, ВОЗМОЖНО, УПОТРЕБЛЯЮТ ЗАПРЕЩЕННЫЕ ПРЕПАРАТЫ. ЗАНИМ.: ППС.',
            '[DATE] ПОСТУПИЛО ТЕЛ.СООБЩЕНИЕ [NAME] [DATE].Р. [ADDRESS] О [NAME], ЧТО ДОМА СКАНДАЛИТ ПЬЯНЫЙ МУЖ. РАБОТАЛИ: ГНР.ЗАЯВЛЕНИЕ НЕ ПОСТУПИЛО, ПРОВЕДЕНА ПРОФ.БЕСЕДА.',
            '[DATE] ПОСТУПИЛО ТЕЛ.СООБЩЕНИЕ [NAME] [DATE].Р. [ADDRESS] О [NAME], ЧТО ДОМА СКАНДАЛИТ ПЬЯНЫЙ МУЖ. РАБОТ.: ГНР.ЗАЯВЛЕНИЕ НЕ ПОСТУПИЛО, ПРОВЕДЕНА ПРОФ.БЕСЕДА.',
            '[DATE] ПОСТУПИЛО ЗАЯВЛЕНИЕ [NAME] [DATE].Р. [ADDRESS]: С. [ADDRESS] . [DATE] ЗАЯВИТЕЛЬ НАХОДЯСЬ НА УЛИЦЕ УТЕРЯЛА СОТ. ТЕЛЕФОН "[NAME]" IMEI:[NAME]. РАБОТАЛИ:[NAME]. МАТЕРИАЛ.',
            '[DATE] ИЗБИЛ БЫВШИЙ МУЖ. ЗАНИМ.:ОУР [NAME]',
            'КУСП 2265 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 19:15 ЗАЯВИТЕЛЬ ГР. [NAME], ,  СООБЩАЕТ, ЧТО ПО [ADDRESS], , У ПОДЪЕЗДА  СБОР МОЛОДЕЖЬ, ЧЕЛОВЕК 10, ШУМЯТ. ЗАНИМАЛИСЬ: ОВО, ППСП.',
            '02 ЗВ. [DATE], [ADDRESS], НЕИЗВЕСТНЫЕ ЛИЦА ПО[ADDRESS] УСТАНОВИЛИ НЕЗАКОННО ВИДЕОКАМЕРУ. ЗАНИМ.',
            'КУСП 6228 ОТ [DATE]. СООБ. ПОСТ. 21.00. ЗАЯвИТЕЛЬ: аноним СООБЩИЛ НА ТЕЛЕФОН ДОВЕРИЯ О [NAME], ЧТО НА [ADDRESS] НА ДЕТСКОЙ ПЛОЩАДКЕ СОТРУДНИК ПОЛИЦИИ В ФОРМЕ ВМЕСТЕ С ЖЕНОЙ И РЕБЕНКОМ РАСПИВАЕТ СПИРТНЫЕ НАПИТКИ. ЗАНИМ. ПА 1513',
            '[DATE] В 01:45 ПРОТОКОЛ ЯВКИ С ПОВИННОЙ ГР. [NAME] О [NAME], ЧТО ПО [ADDRESS] СОВЕРШИЛ ОТКРЫТОЕ ХИЩЕНИЕ ЗОЛОТА.ЗАНИМ.: ОУР [NAME]',
            'ГЛОНАСС+112 СООБЩИЛ О ВОЗГОРАНИИ ЧАСТНЫХ ДОМОВ С.[NAME] ЗА КИРПИЧНЫМ ЗАВОДОМ. ПЧ-112 ВЫЕХАЛИ, ГОРИТ СОЛОМА В [NAME].',
            '[DATE] В 18.14 ПОСТУПИЛО СООБЩЕНИЕ "03" [NAME] О [NAME], ЧТО [DATE] В 17.30 В ПРИЕМНЫЙ ПОКОЙ [NAME] ЦРБ ДОСТАВЛЕНА ГР.[NAME]], [DATE].Р. [ADDRESS], Д/З ТРАВМАТИЧЕСКИЙ ПЕРЕЛОМ НИЖНЕЙ ЧЕЛЮСТИ СПРАВА БЕЗ СМЕЩЕНИЯ, СО СЛОВ УПАЛА [DATE] В[ADDRESS]. ВЫЕХАЛ В 18.17 УУП [NAME], [NAME]. ПРОВЕРОЧНЫЙ МАТЕРИАЛ.',
            'Диспетчер ПЦО ОВО по [ADDRESS] филиала ФГКУ УВО МВД по РТ [NAME] сообщила, что [DATE] 16.09 часов в[NAME] молкомбинате сработала КТС. Технические неполадки. Выехали: [NAME], [NAME], ГИБДД-[NAME], [NAME]',
            '[ADDRESS], напротив здания мирового суда, а/м рено ., перегородила проезд, з-ль не может выехать, заявитель [DATE], [ADDRESS] ул. [ADDRESS]',
            '[NAME], [DATE]г.рожд., [ADDRESS] СООБЩИЛ, ЧТО ГАРАЖНЫЙ КОМПЛЕКС " [NAME]", ГАРАЖ № , [DATE]. ОБНАРУЖИЛ ПРОПАЛ ВЕЛОСИПЕД " [NAME]" ЧЕРНОГО [NAME].   ВЫЕЗЖАЛИ:  [NAME]-СО,  ОУР-[NAME],  [NAME]-УУП, [NAME]-198.',
            'Старший ГЗ ПЦО ОВО ст. сержант полиции [NAME] сообщил о том, что по [ADDRESS]. [NAME], [DATE]. р. в состоянии алкогольного опьянения устроил скандал и пытался поджечь квартиру. Силами сотрудников ОВО очаг возгорания локализован.[NAME] задержан экипажем ОВО. Выехали ответсвенный по ОМВД [NAME], УУП [NAME] , СОГ.',
        ]
        # TODO: Захватывает лишние точки
        self.answers = [
            ['ЗАНИМ.:ППС.'],
            ['РАБОТАЛИ:ГНР.'],
            ['РАБОТ.:ГНР.'],
            ['РАБОТАЛИ:[NAME].'],
            ['ЗАНИМ.:ОУР[NAME]'],
            ['ЗАНИМАЛИСЬ:ОВО,ППСП.'],
            ['ЗАНИМ.'],
            ['ЗАНИМ.ПА1513'],
            ['ЗАНИМ.:ОУР[NAME]'],
            ['ВЫЕХАЛИ,'],  # TODO: ошибка ' ПЧ-112 Выехали'
            ['ВЫЕХАЛВ18.17УУП[NAME],[NAME].'],
            ['Выехали:[NAME],[NAME],'],  # TODO: Выехали:[NAME],[NAME],ГИБДД-[NAME],[NAME]   ГИБДД слишком длинная аббр
            ['выехать,'],  # TODO: ошибка 'выехать,' -- лишнее
            ['ВЫЕЗЖАЛИ:[NAME]-СО,ОУР-[NAME],[NAME]-УУП,[NAME]-198.'],
            ['Выехали'],  # TODO: не работает 'ВыехалиответсвенныйпоОМВД[NAME],УУП[NAME],СОГ.'
        ]
        # TODO: Захватывает лишние точки
        self.facts = [
            [{'items': [{'position': 'ППС', 'name': None}], 'time': None}],
            [{'items': [{'position': 'ГНР', 'name': None}], 'time': None}],
            [{'items': [{'position': 'ГНР', 'name': None}], 'time': None}],
            [{'items': [{'position': None, 'name': '[NAME]'}], 'time': None}],
            [{'items': [{'position': 'ОУР', 'name': '[NAME]'}], 'time': None}],
            [{'items': [{'position': 'ОВО', 'name': None}, {'position': 'ППСП', 'name': None}], 'time': None}],
            [{'items': [], 'time': None}],
            [{'items': [{'position': 'ПА 1513', 'name': None}], 'time': None}],
            [{'items': [{'position': 'ОУР', 'name': '[NAME]'}], 'time': None}],
            [{'items': [], 'time': None}],  # TODO: ошибка ' ПЧ-112 Выехали'
            [
                 {'items': [
                     {'position': 'УУП', 'name': '[NAME]'},
                     {'position': None, 'name': '[NAME]'}
                 ],
                 'time': {'hours': 18, 'minutes': 17}}
             ],
            [
                 {'items': [
                     {'position': None, 'name': '[NAME]'},
                     {'position': None, 'name': '[NAME]'},
                 ],
                 'time': None}
            ],  # TODO: Выехали:[NAME],[NAME],ГИБДД-[NAME],[NAME]   ГИБДД слишком длинная аббр
            [{'items': [], 'time': None}],  # TODO: ошибка 'выехать,' -- лишнее
            [
                 {'items': [
                     {'position': 'СО', 'name': '[NAME]'},
                     {'position': 'ОУР', 'name': '[NAME]'},
                     {'position': 'УУП', 'name': '[NAME]'},
                     {'position': '198', 'name': '[NAME]'},
                 ],
                 'time': None}
            ],
            [{'items': [], 'time': None}],  # TODO: не работает 'ВыехалиответсвенныйпоОМВД[NAME],УУП[NAME],СОГ.'
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == run_by
        if len(fact.items) == 0:
            items_bool = len(target_fact['items']) == 0
        else:
            items_bool = len(fact.items) == len(target_fact['items'])
            for item, target_item in zip(fact.items, target_fact['items']):
                is_valid_type &= type(item) == run_by_item
                items_bool &= item.position == target_item['position'] and (
                    item.name is None and target_item['name'] is None or
                    item.name is not None and item.name.name == target_item['name']
                )
        time_bool = fact.time is None and target_fact['time'] is None or fact.time is not None and \
                    fact.time.hours == target_fact['time']['hours'] and fact.time.minutes == target_fact['time']['minutes']
        return items_bool and time_bool and is_valid_type

    def test(self):
        self.base.base_test(Parser(RUN_BY), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
