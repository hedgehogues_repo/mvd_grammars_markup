import unittest

from yargy import Parser

from internal.interpretations import person
from internal.grammatics.other.birthday import BIRTHDAY
from test.base import BaseTest


class TestBirthday(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'по факту смерти [DATE] [NAME]-[DATE] г/р г. [ADDRESS]',
            '[DATE]. в 07.30 ч. обратилась ,  о том, что стучит посторонний.  Проверочный материал.',
            '[DATE]. в 07.30 ч. обратилась [NAME][DATE]гр,  о том, что стучит посторонний.  Проверочный материал.',
            'ЗАЯВИТЕЛЬ [NAME][ADDRESS] В СТОРОНУ [NAME] В АВТОБУСЕ 89 МАРШРУТА ПРИ РЕЗКОМ ТОРМОЖЕНИИ В САЛОНЕ УПАЛА ЖЕНЩИНА',
            'КУСП 3810 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 15:54 ВХ. МАТЕРИАЛ 1804. ЗАЯВИТЕЛЬ ГР.[NAME][DATE], Г.Р. ПО ФАКТУ МОШЕННИЧЕСКИХ ДЕЙСТВИЙ СО СТОРОНЫ КПК "СБЕРФИНАНС" ПО [ADDRESS]',
            'Сообщение [NAME][DATE].р.,прож.[ADDRESS] ,о том,что [DATE] по месту жительства скончалась [NAME] [DATE].р.,[ADDRESS],пенсионерка. Труп без ВПНС.',
            'Сообщение Заявитель [NAME][DATE].р.,прож.[ADDRESS] ,о том,что [DATE] по месту жительства скончалась [NAME] [DATE].р.,[ADDRESS],пенсионерка. Труп без ВПНС.',
            'обратился [NAME] [DATE]/р заокский р-он д.[ADDRESS] с телесными повреждениями',
            'КУСП 2265 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 19:15 ЗАЯВИТЕЛЬ ГР. [NAME], ,  СООБЩАЕТ, ЧТО ПО [ADDRESS], , У ПОДЪЕЗДА  СБОР МОЛОДЕЖЬ, ЧЕЛОВЕК 10, ШУМЯТ. ЗАНИМАЛИСЬ: ОВО, ППСП.',
            'ВХОДЯЩИЙ Г-3 ИЗ ПРОКУРАТУРЫ ПО ЗАЯВЛЕНИЮ [DATE].Р.,[ADDRESS] ПО ФАКТУ МОШЕННИЧЕСТВА',
            'Рапорт д/Ч [NAME],по факту доставления в ОП №15 "[NAME]" гр.[NAME]] [DATE].р.,у которого имелись телесные повреждения в области лица.Со слов гр.[NAME] данные телесные повреждения он получил [DATE]. на [ADDRESS] по дороге домой,т.к. находился в н/с упал и ударился головой об асфальт.Его никто не избивал,заявления не поступило,от прохождения СМЭ отказался.',
            'ПО ТЕЛЕФОНУ [NAME], [DATE]г., ЧТО В Д.[ADDRESS] ХУЛИГАНИТ ПЬЯНЫЙ МУЖ [NAME], УГРОЖАЛ ОХОТНИЧЬЕМ РУЖЬЕМ.',
            'ЗАЯВЛЕНИЕ ГР. [NAME], [DATE].Р., [ADDRESS]  РАБ. МАГ. "[NAME]", О [NAME], ЧТО [DATE]. В 06.40ЧАС. В МАГ. "[NAME]", ПО [ADDRESS], ГР. [NAME] , [DATE].Р., [ADDRESS] , ВЫРАЖАЛСЯ НЕЦЕНЗУРНОЙ БРАНЬЮ, ПЫТАЛСЯ УЧИНИТЬ ДРАКУ. ЗАРЕГ. В 08.00ЧАС. ЗАНИМ: ПА-262 [NAME].',
            'Т/С [NAME], [DATE].Р. [ADDRESS][DATE] [TIME] С.[NAME] ДВОРЕ ЦРБ ПРОИЗОШЛО ДТП. СОСТАВЛЕН АДМ. ПРОТОКОЛ.',
            'ЗВ. [NAME], [DATE] [ADDRESS] . В ПОД. 7 МЕЖДУ 2-3 ЭТ. БОМЖ, СПИТ В А/О.',
            'ЗВ. [NAME][DATE] [ADDRESS] . В ПОД. 7 МЕЖДУ 2-3 ЭТ. БОМЖ, СПИТ В А/О.',
            '12:15 из СМП РБ [NAME] сообщила о том, что по [ADDRESS]покончил жизнью самоубийством через повешение гражданин [NAME], [DATE] рождения.',
            'ПОЧТА ВХ. №5102. ЛИУ-1 [NAME]. МАТЕРИАЛ ПРОВЕРКИ /КУСП-29 ЛИУ-1/ ОТ [DATE]. ПО ФАКТУ ОБРАЩЕНИЯ В МЕД. ЧАСТЬ [NAME] [DATE].Р. ЗАРЕГ. В 17.02 ЧАС. [DATE]. ЗАНИМ: УУП [NAME]',
            '[DATE] В 10:57 ЗАЯВИТЕЛЬ ГР. [NAME],[DATE].Р. О [NAME], ЧТО ПО [ADDRESS] НЕ ОТКРЫВАЮТ ДВЕРЬ, В КВАРТИРЕ НАХОДИТСЯ МУЖЧИНА.ЗАНИМ.: УУП [NAME]',
            '[DATE].р. [ADDRESS]  о том, что сожитель нанес побои. Приобщено КУСП-19755.',
        ]
        self.answers = [
            ['[NAME]-[DATE]г/р'],
            [],
            ['[NAME][DATE]гр'],
            [],
            ['[NAME][DATE],Г.Р.', '[DATE]ГОДА'],
            ['[NAME][DATE].р.', '[NAME][DATE].р.'],
            ['[NAME][DATE].р.', '[NAME][DATE].р.'],
            ['[NAME][DATE]/р'],
            ['[DATE]ГОДА'],  # TODO: Warning
            ['[DATE].Р.'],
            ['[NAME]][DATE].р.'],
            ['[NAME],[DATE]г.'],
            ['[NAME],[DATE].Р.', '[NAME],[DATE].Р.'],
            ['[NAME],[DATE].Р.'],
            ['[NAME],[DATE]'],
            ['[NAME][DATE]'],
            ['[NAME],[DATE]рождения.'],
            ['[NAME][DATE].Р.'],
            ['[NAME],[DATE].Р.'],
            ['[DATE].р.'],
        ]
        self.facts = [
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [],
            [
                {'name': '[NAME]', 'date': '[DATE]'},
                {'name': None, 'date': '[DATE]'},
            ],
            [
                {'name': '[NAME]', 'date': '[DATE]'},
                {'name': '[NAME]', 'date': '[DATE]'},
            ],
            [
                {'name': '[NAME]', 'date': '[DATE]'},
                {'name': '[NAME]', 'date': '[DATE]'},
            ],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [{'name': None, 'date': '[DATE]'}],
            [{'name': None, 'date': '[DATE]'}],
            [{'name': '[NAME]]', 'date': '[DATE]'}],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [
                {'name': '[NAME]', 'date': '[DATE]'},
                {'name': '[NAME]', 'date': '[DATE]'},
            ],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [{'name': '[NAME]', 'date': '[DATE]'}],
            [{'name': None, 'date': '[DATE]'}],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        date_bool = fact.birthday is None and target_fact['date'] is None or \
                    fact.birthday.date == target_fact['date']
        name_bool = fact.name is None and target_fact['name'] is None or \
                    fact.name.name == target_fact['name']
        address_bool = fact.address is None
        is_valid_type = type(fact) == person
        return date_bool and name_bool and address_bool and is_valid_type

    def test(self):
        self.base.base_test(Parser(BIRTHDAY), self.tests, self.answers, self.facts,  self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
