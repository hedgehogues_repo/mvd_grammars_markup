import unittest

from yargy import Parser

from internal.interpretations import person
from internal.grammatics.reported_by import REPORTED_BY
from test.base import BaseTest


class TestAttachedTo(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            '[DATE]. в 07.30 ч. обратилась [NAME][DATE]гр,  о том, что стучит посторонний.  Проверочный материал.',
            'по факту смерти [DATE] [NAME]-[DATE] г/р г. [ADDRESS]',
            'ЗАЯВИТЕЛЬ [NAME][ADDRESS] В СТОРОНУ [NAME] В АВТОБУСЕ 89 МАРШРУТА ПРИ РЕЗКОМ ТОРМОЖЕНИИ В САЛОНЕ УПАЛА ЖЕНЩИНА',
            'КУСП 3810 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 15:54 ВХ. МАТЕРИАЛ 1804. ЗАЯВИТЕЛЬ ГР.[NAME][DATE], Г.Р. ПО ФАКТУ МОШЕННИЧЕСКИХ ДЕЙСТВИЙ СО СТОРОНЫ КПК "СБЕРФИНАНС" ПО [ADDRESS]',
            'СООБЩЕНИЕ ПО ТЕЛЕФОНУ "02". ЗАЯВИТЕЛЬ [NAME], [ADDRESS]. БРАТ ПЬЯНЫЙ ХУЛИГАНИТ.',
            '02. З-ЛЬ [NAME],[NAME], [ADDRESS] МАГАЗИН ПРОДУКТЫ, ЗАДЕРЖАНЫ 2 ПАРНЕЙ ЗА МЕЛКОЕ ХИЩЕНИЕ',
            '02. З-ЛЬ [NAME],[ADDRESS] МАГАЗИН ПРОДУКТЫ, ЗАДЕРЖАНЫ 2 ПАРНЕЙ ЗА МЕЛКОЕ ХИЩЕНИЕ',
            'Сообщение [NAME][DATE].р.,прож.[ADDRESS] ,о том,что [DATE] по месту жительства скончалась [NAME] [DATE].р.,[ADDRESS],пенсионерка. Труп без ВПНС.',
            'Сообщение Заявитель [NAME][DATE].р.,прож.[ADDRESS] ,о том,что [DATE] по месту жительства скончалась [NAME] [DATE].р.,[ADDRESS],пенсионерка. Труп без ВПНС.',
            'Сообщение [NAME][DATE].р.,прож.[ADDRESS] Заявитель: [NAME],о том,что [DATE] по месту жительства скончалась [NAME] [DATE].р.,[ADDRESS],пенсионерка. Труп без ВПНС.',
            'обратился [NAME] [DATE]/р заокский р-он д.[ADDRESS] с телесными повреждениями',
            'КУСП 2265 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 19:15 ЗАЯВИТЕЛЬ ГР. [NAME], ,  СООБЩАЕТ, ЧТО ПО [ADDRESS], , У ПОДЪЕЗДА  СБОР МОЛОДЕЖЬ, ЧЕЛОВЕК 10, ШУМЯТ. ЗАНИМАЛИСЬ: ОВО, ППСП.',
            # 'КУСП 8615  [DATE] В 01:02 ЗАЯВИТЕЛЬ АНОНИМ О [NAME], ЧТО ОКОЛО ТРАНСФОРМАТОРНОЙ БУДКИ ВО ДВОРЕ ДОМА 71 ПО [ADDRESS] НЕСКОЛЬКО ЧЕЛОВЕК В ЧЕРНОЙ [NAME] ШУМЯТ, СЛУШАЮТ ГРОМКУЮ МУЗЫКУ, ВОЗМОЖНО, УПОТРЕБЛЯЮТ ЗАПРЕЩЕННЫЕ ПРЕПАРАТЫ. ЗАНИМ.: ППС.',
            '02 ЗВ. [DATE], [ADDRESS], НЕИЗВЕСТНЫЕ ЛИЦА ПО[ADDRESS] УСТАНОВИЛИ НЕЗАКОННО ВИДЕОКАМЕРУ. ЗАНИМ.',
            'сообщение Глонасс 112: вломился в окно мужчина с. [ADDRESS] . Заявитель [NAME]',
            'ВХОДЯЩИЙ Г-3 ИЗ ПРОКУРАТУРЫ ПО ЗАЯВЛЕНИЮ [DATE].Р.,[ADDRESS] ПО ФАКТУ МОШЕННИЧЕСТВА',
            'Сообщение "02" [ADDRESS] заявитель просит полицию.',
            'Рапорт д/Ч [NAME],по факту доставления в ОП №15 "[NAME]" гр.[NAME]] [DATE].р.,у которого имелись телесные повреждения в области лица.Со слов гр.[NAME] данные телесные повреждения он получил [DATE]. на [ADDRESS] по дороге домой,т.к. находился в н/с упал и ударился головой об асфальт.Его никто не избивал,заявления не поступило,от прохождения СМЭ отказался.',
            'Рапорт [NAME],по факту доставления в ОП №15 "[NAME]" гр.[NAME]] [DATE].р.,у которого имелись телесные повреждения в области лица.Со слов гр.[NAME] данные телесные повреждения он получил [DATE]. на [ADDRESS] по дороге домой,т.к. находился в н/с упал и ударился головой об асфальт.Его никто не избивал,заявления не поступило,от прохождения СМЭ отказался.',
            # 'КУСП № 13362 от [DATE], сообщение зарегистрировано в 01.54. сообщение со службы "112" через оператора "02": [ADDRESS], шумная компания, распивают спиртные напитки, на замечания не реагируют, заявитель не представился. Номер происшествия в системе 112: 1370692-с\\',
            # 'КУСП № 13362 от [DATE]., сообщение зарегистрировано в 01.54. сообщение со службы "112" через оператора "02": [ADDRESS], шумная компания, распивают спиртные напитки, на замечания не реагируют, заявитель не представился. Номер происшествия в системе 112: 1370692-с\\',
            'ЗАЯВЛЕНИЕ ГР. [NAME], [DATE].Р., [ADDRESS]  РАБ. МАГ. "[NAME]", О [NAME], ЧТО [DATE]. В 06.40ЧАС. В МАГ. "[NAME]", ПО [ADDRESS], ГР. [NAME] , [DATE].Р., [ADDRESS] , ВЫРАЖАЛСЯ НЕЦЕНЗУРНОЙ БРАНЬЮ, ПЫТАЛСЯ УЧИНИТЬ ДРАКУ. ЗАРЕГ. В 08.00ЧАС. ЗАНИМ: ПА-262 [NAME].',
            'Т/С [NAME], [DATE].Р. [ADDRESS][DATE] [TIME] С.[NAME] ДВОРЕ ЦРБ ПРОИЗОШЛО ДТП. СОСТАВЛЕН АДМ. ПРОТОКОЛ.',
            'ЗАЯВЛЕНИЕ [NAME][DATE] [ADDRESS]  РАБ. МАГ. "[NAME]", О [NAME], ЧТО [DATE]. В 06.40ЧАС. В МАГ. "[NAME]", ПО [ADDRESS], ГР. [NAME] , [DATE].Р., [ADDRESS] , ВЫРАЖАЛСЯ НЕЦЕНЗУРНОЙ БРАНЬЮ, ПЫТАЛСЯ УЧИНИТЬ ДРАКУ. ЗАРЕГ. В 08.00ЧАС. ЗАНИМ: ПА-262 [NAME].',
            'ЗАЯВЛЕНИЕ [NAME] [ADDRESS] [DATE]   РАБ. МАГ. "[NAME]", О [NAME], ЧТО [DATE]. В 06.40ЧАС. В МАГ. "[NAME]", ПО [ADDRESS], ГР. [NAME] , [DATE].Р., [ADDRESS] , ВЫРАЖАЛСЯ НЕЦЕНЗУРНОЙ БРАНЬЮ, ПЫТАЛСЯ УЧИНИТЬ ДРАКУ. ЗАРЕГ. В 08.00ЧАС. ЗАНИМ: ПА-262 [NAME].',
            'ЗВ. [NAME] [DATE] [ADDRESS] . В ПОД. 7 МЕЖДУ 2-3 ЭТ. БОМЖ, СПИТ В А/О.',
            'ЗВ. [NAME],[DATE] [ADDRESS] . В ПОД. 7 МЕЖДУ 2-3 ЭТ. БОМЖ, СПИТ В А/О.',
            '12:15 из СМП РБ [NAME] сообщила о том, что по [ADDRESS]покончил жизнью самоубийством через повешение гражданин [NAME], [DATE] рождения.',
        ]
        self.answers = [
            ['.обратилась[NAME][DATE]гр'],
            [],
            ['ЗАЯВИТЕЛЬ[NAME][ADDRESS]'],
            ['.ЗАЯВИТЕЛЬГР.[NAME][DATE],Г.Р.', '[DATE]ГОДАСООБЩЕНИЕ'],  # TODO: '[DATE]ГОДАСООБЩЕНИЕ' -- лишнее
            ['.ЗАЯВИТЕЛЬ[NAME],[ADDRESS]'],
            ['.З-ЛЬ[NAME]'],  # TODO: Не работает З-ЛЬ [NAME],[NAME], [ADDRESS]
            ['.З-ЛЬ[NAME],[ADDRESS]'],
            ['Сообщение[NAME][DATE].р.,прож.[ADDRESS]'],
            ['Заявитель[NAME][DATE].р.,прож.[ADDRESS]'],
            ['[NAME][DATE].р.,прож.[ADDRESS]Заявитель:'],
            ['обратился[NAME][DATE]/р'],
            ['ГР.[NAME],,СООБЩАЕТ,', '[DATE]ГОДАСООБЩЕНИЕ'],
            ['ЗВ.[DATE],[ADDRESS]'],
            ['с.[ADDRESS].Заявитель[NAME]'],
            ['ЗАЯВЛЕНИЮ[DATE].Р.,[ADDRESS]'],
            ['[ADDRESS]заявитель'],
            [],  # TODO: не работает 'Рапорт д/Ч [NAME]'
            ['Рапорт[NAME]'],
            ['ЗАЯВЛЕНИЕГР.[NAME],[DATE].Р.,[ADDRESS]'],
            [],
            ['ЗАЯВЛЕНИЕ[NAME][DATE][ADDRESS]'],
            ['ЗАЯВЛЕНИЕ[NAME][ADDRESS]'],  # TODO: теряется [DATE] из 'ЗАЯВЛЕНИЕ[NAME][ADDRESS][DATE]'
            ['ЗВ.[NAME][DATE][ADDRESS]'],
            ['ЗВ.[NAME],[DATE][ADDRESS]'],
            ['[NAME]сообщилао'],  # TODO: лишний предлог
        ]

        self.facts = [
            [{'name': '[NAME]', 'date': '[DATE]', 'address': None}],
            [],
            [{'name': '[NAME]', 'date': None, 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [
                {'name': '[NAME]', 'date': '[DATE]', 'address': None},
                {'name': None, 'date': '[DATE]', 'address': None},
            ],  # TODO: '[DATE]ГОДАСООБЩЕНИЕ' -- лишнее
            [{'name': '[NAME]', 'date': None, 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': '[NAME]', 'date': None, 'address': None}],  # TODO: Не работает З-ЛЬ [NAME],[NAME], [ADDRESS]
            [{'name': '[NAME]', 'date': None, 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': '[NAME]', 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': '[NAME]', 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [
                {'name': '[NAME]', 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}},
            ],
            [{'name': '[NAME]', 'date': '[DATE]', 'address': None}],
            [
                {'name': '[NAME]', 'date': None, 'address': None},
                {'name': None, 'date': '[DATE]', 'address': None},
            ],
            [{'name': None, 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': '[NAME]', 'date': None, 'address': {'prefix': 'с.', 'address': '[ADDRESS]'}}],
            [{'name': None, 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': None, 'date': None, 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [],  # TODO: не работает 'Рапорт д/Ч [NAME]'
            [{'name': '[NAME]', 'date': None, 'address': None}],
            [{'name': '[NAME]', 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [],
            [{'name': '[NAME]', 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': '[NAME]', 'date': None, 'address': {'prefix': None, 'address': '[ADDRESS]'}}],  # TODO: теряется [DATE] из 'ЗАЯВЛЕНИЕ[NAME][ADDRESS][DATE]'
            [{'name': '[NAME]', 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': '[NAME]', 'date': '[DATE]', 'address': {'prefix': None, 'address': '[ADDRESS]'}}],
            [{'name': '[NAME]', 'date': None, 'address': None}],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        date_bool = fact.birthday is None and target_fact['date'] is None or fact.birthday.date == target_fact['date']
        address_bool = fact.address is None and target_fact['address'] is None or \
            fact.address.address is not None and fact.address.address == target_fact['address']['address'] and \
            fact.address.prefix == target_fact['address']['prefix']
        name_bool = fact.name is None and target_fact['name'] is None or \
            fact.name is not None and fact.name.name == target_fact['name']
        is_valid_type = type(fact) == person
        return date_bool and name_bool and address_bool and is_valid_type

    def test(self):
        self.base.base_test(Parser(REPORTED_BY), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
