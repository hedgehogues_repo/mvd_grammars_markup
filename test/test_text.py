import unittest

from yargy import Parser

from internal.grammatics.text import TEXT
from internal.interpretations import text
from test.base import BaseTest


class TestTime(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'КУСП 3810 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 15 час 54 мин ВХ. МАТЕРИАЛ 1804. ЗАЯВИТЕЛЬ ГР.[NAME][DATE], Г.Р. ПО ФАКТУ МОШЕННИЧЕСКИХ ДЕЙСТВИЙ СО СТОРОНЫ КПК "СБЕРФИНАНС" ПО [ADDRESS]',
            'ПОЧТА ВХ. №5102. ЛИУ-1 [NAME]. МАТЕРИАЛ ПРОВЕРКИ /КУСП-29 ЛИУ-1/ ОТ [DATE]. ПО ФАКТУ ОБРАЩЕНИЯ В МЕД. ЧАСТЬ [NAME] [DATE].Р. ЗАРЕГ. В 17-02 ЧАС. [DATE]. ЗАНИМ: УУП [NAME]',
            '[DATE] В 10:57 ЗАЯВИТЕЛЬ ГР. [NAME],[DATE].Р. О [NAME], ЧТО ПО [ADDRESS] НЕ ОТКРЫВАЮТ ДВЕРЬ, В КВАРТИРЕ НАХОДИТСЯ МУЖЧИНА.ЗАНИМ.: УУП [NAME]',
        ]
        self.answers = [
            ['ПОФАКТУМОШЕННИЧЕСКИХДЕЙСТВИЙСОСТОРОНЫ'],
            ['ПОФАКТУОБРАЩЕНИЯВМЕД.ЧАСТЬ'],
            [],
        ]
        self.facts = [
            ['мошеннический действие с сторона'],
            ['обращение в мёд. часть'],
            [],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        text_bool = fact.text == target_fact
        is_valid_type = type(fact) == text
        return text_bool and is_valid_type

    def test(self):
        self.base.base_test(Parser(TEXT), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
