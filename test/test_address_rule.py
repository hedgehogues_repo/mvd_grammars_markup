import unittest

from yargy import Parser

from internal.interpretations import address
from internal.grammatics.other.address import ADDRESS_RULE
from test.base import BaseTest


class TestAddressRule(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            '[DATE]. в 07.30 ч. обратилась [NAME][DATE]гр,  о том, что стучит посторонний.  Проверочный материал.',
            'по факту смерти [DATE] [NAME]-[DATE] г/р дер [ADRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р дер [ADDRES]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р г. [ADDRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р г. ADDRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р г. [ADDRESS',
            'по факту смерти [DATE] [NAME]-[DATE] г/р г. [[ADDRESS]]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р эт.[ADDRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р эт [ADDRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р г [ADDRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р д. [ADDRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р д [ADDRESS]',
            'по факту смерти [DATE] [NAME]-[DATE] г/р дер [ADDRESS]',
            'Сообщение [NAME][DATE].р.,прож.[ADDRESS] Заявитель: [NAME],о том,что [DATE] по месту жительства скончалась [NAME] [DATE].р.,[ADDRESS],пенсионерка. Труп без ВПНС.'
        ]
        self.answers = [
            [],
            ['дер[ADRESS]'],
            ['дер[ADDRES]'],
            ['г.[ADDRESS]'],
            ['г.ADDRESS]'],
            ['г.[ADDRESS'],
            ['г.[[ADDRESS]]'],
            ['эт.[ADDRESS]'],
            ['эт[ADDRESS]'],
            ['г[ADDRESS]'],
            ['д.[ADDRESS]'],
            ['д[ADDRESS]'],
            ['дер[ADDRESS]'],
            ['прож.[ADDRESS]', '[ADDRESS]'],
        ]
        self.facts = [
            [],
            [{'prefix': 'дер', 'address': '[ADRESS]'}],
            [{'prefix': 'дер', 'address': '[ADDRES]'}],
            [{'prefix': 'г.', 'address': '[ADDRESS]'}],
            [{'prefix': 'г.', 'address': 'ADDRESS]'}],
            [{'prefix': 'г.', 'address': '[ADDRESS'}],
            [{'prefix': 'г.', 'address': '[[ADDRESS]]'}],
            [{'prefix': 'эт.', 'address': '[ADDRESS]'}],
            [{'prefix': 'эт', 'address': '[ADDRESS]'}],
            [{'prefix': 'г', 'address': '[ADDRESS]'}],
            [{'prefix': 'д.', 'address': '[ADDRESS]'}],
            [{'prefix': 'д', 'address': '[ADDRESS]'}],
            [{'prefix': 'дер', 'address': '[ADDRESS]'}],
            [
                {'prefix': None, 'address': '[ADDRESS]'},
                {'prefix': None, 'address': '[ADDRESS]'}
            ],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == address
        is_prefix = fact.prefix is None and target_fact['prefix'] is None or \
            fact.prefix is not None and target_fact['prefix'] == fact.prefix
        is_adress = fact.address == target_fact['address']
        return is_prefix and is_adress and is_valid_type

    def test(self):
        self.base.base_test(Parser(ADDRESS_RULE), self.tests, self.answers, self.facts, TestAddressRule.fact_assertion)


if __name__ == '__main__':
    unittest.main()
