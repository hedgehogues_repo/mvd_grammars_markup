import unittest

from yargy import Parser

from internal.grammatics.other.time import TIME
from internal.interpretations import time
from test.base import BaseTest


class TestTime(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            'В связи со смертью владельца оружия [NAME], сдано рружьё МЦ 21-12 МР на хранение.',
            '[DATE]. в 07.30 ч. обратилась ,  о том, что стучит посторонний.  Проверочный материал.',
            '[DATE]. в 07,30 ч. обратилась [NAME][DATE]гр,  о том, что стучит посторонний.  Проверочный материал.',
            '[DATE]. в 07 час - 30 м обратилась [NAME][DATE]гр,  о том, что стучит посторонний.  Проверочный материал.',
            '[DATE]. в 07 час 30 час обратилась [NAME][DATE]гр,  о том, что стучит посторонний.  Проверочный материал.',
            '[DATE]. в 7 30 час обратилась [NAME][DATE]гр,  о том, что стучит посторонний.  Проверочный материал.',
            'ЗАЯВИТЕЛЬ [NAME][ADDRESS] В СТОРОНУ [NAME] В АВТОБУСЕ 89 МАРШРУТА ПРИ РЕЗКОМ ТОРМОЖЕНИИ В САЛОНЕ УПАЛА ЖЕНЩИНА',
            'КУСП 3810 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 15 час 54 мин ВХ. МАТЕРИАЛ 1804. ЗАЯВИТЕЛЬ ГР.[NAME][DATE], Г.Р. ПО ФАКТУ МОШЕННИЧЕСКИХ ДЕЙСТВИЙ СО СТОРОНЫ КПК "СБЕРФИНАНС" ПО [ADDRESS]',
            'КУСП 2265 ОТ [DATE] ГОДА СООБЩЕНИЕ ПОСТУПИЛО 19 ч 15 м ЗАЯВИТЕЛЬ ГР. [NAME], ,  СООБЩАЕТ, ЧТО ПО [ADDRESS], , У ПОДЪЕЗДА  СБОР МОЛОДЕЖЬ, ЧЕЛОВЕК 10, ШУМЯТ. ЗАНИМАЛИСЬ: ОВО, ППСП.',
            'ЗАЯВЛЕНИЕ ГР. [NAME], [DATE].Р., [ADDRESS]  РАБ. МАГ. "[NAME]", О [NAME], ЧТО [DATE]. В 06.40ЧАС. В МАГ. "[NAME]", ПО [ADDRESS], ГР. [NAME] , [DATE].Р., [ADDRESS] , ВЫРАЖАЛСЯ НЕЦЕНЗУРНОЙ БРАНЬЮ, ПЫТАЛСЯ УЧИНИТЬ ДРАКУ. ЗАРЕГ. В 08.00ЧАС. ЗАНИМ: ПА-262 [NAME].',
            'ЗВ. [NAME], [DATE] [ADDRESS] . В ПОД. 7 МЕЖДУ 2-3 ЭТ. БОМЖ, СПИТ В А/О.',
            '12:15 из СМП РБ [NAME] сообщила о том, что по [ADDRESS]покончил жизнью самоубийством через повешение гражданин [NAME], [DATE] рождения.',
            'ПОЧТА ВХ. №5102. ЛИУ-1 [NAME]. МАТЕРИАЛ ПРОВЕРКИ /КУСП-29 ЛИУ-1/ ОТ [DATE]. ПО ФАКТУ ОБРАЩЕНИЯ В МЕД. ЧАСТЬ [NAME] [DATE].Р. ЗАРЕГ. В 17-02 ЧАС. [DATE]. ЗАНИМ: УУП [NAME]',
            '[DATE] В 10:57 ЗАЯВИТЕЛЬ ГР. [NAME],[DATE].Р. О [NAME], ЧТО ПО [ADDRESS] НЕ ОТКРЫВАЮТ ДВЕРЬ, В КВАРТИРЕ НАХОДИТСЯ МУЖЧИНА.ЗАНИМ.: УУП [NAME]',
        ]
        self.answers = [
            ['21-12'],  # TODO: Warning -- ответ неверный, выявить сложно
            ['07.30ч.'],
            ['07,30ч.'],
            [],
            ['07час30час'],  # TODO: Warning -- ответ неверный, но для упрощения правил, оставим
            ['730час'],
            [],
            ['15час54мин'],
            ['19ч15м'],
            ['06.40ЧАС.', '08.00ЧАС.'],
            ['2-3'],  # TODO: Warning -- ответ неверный, выявить сложно
            ['12:15'],
            ['17-02ЧАС.'],
            ['10:57'],
            # TODO: Нет правил для времени, написанного буквами
        ]
        self.facts = [
            [{'hours': 21, 'minutes': 12}],  # TODO: Warning -- ответ неверный, выявить сложно
            [{'hours': 7, 'minutes': 30}],
            [{'hours': 7, 'minutes': 30}],
            [],
            [{'hours': 7, 'minutes': 30}],  # TODO: Warning -- ответ неверный, но для упрощения правил, оставим
            [{'hours': 7, 'minutes': 30}],
            [],
            [{'hours': 15, 'minutes': 54}],
            [{'hours': 19, 'minutes': 15}],
            [{'hours': 6, 'minutes': 40}, {'hours': 8, 'minutes': 0}],
            [{'hours': 2, 'minutes': 3}],  # TODO: Warning -- ответ неверный, выявить сложно
            [{'hours': 12, 'minutes': 15}],
            [{'hours': 17, 'minutes': 2}],
            [{'hours': 10, 'minutes': 57}],
            # TODO: Нет правил для времени, написанного буквами
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == time
        return fact.hours == target_fact['hours'] and fact.minutes == target_fact['minutes'] and is_valid_type

    def test(self):
        self.base.base_test(Parser(TIME), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
