import unittest

from yargy import Parser

from internal.grammatics.other.time_interval import TIME_INTERVAL
from internal.interpretations import interval
from test.base import BaseTest


class TestInterval(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            '[DATE] в период с 09.00 час. до 15.00 час. неизветсные из квартиры заявительницы похитили сумку с документами и банковскими карточками.',
            'ПОЧТА ВХ. №5102. ЛИУ-1 [NAME]. МАТЕРИАЛ ПРОВЕРКИ /КУСП-29 ЛИУ-1/ ОТ [DATE]. ПО ФАКТУ ОБРАЩЕНИЯ В МЕД. ЧАСТЬ [NAME] [DATE].Р. ЗАРЕГ. В 17-02 ЧАС. [DATE]. ЗАНИМ: УУП [NAME]',
            '[DATE]. в период времени с 16ч. 00мин. до 17ч. 00мин. [NAME], [ADDRESS],  оставил свой с/т у знакомого на [ADDRESS],',
        ]
        self.answers = [
            ['впериодс09.00час.до15.00час.'],
            [],
            ['впериодвременис16ч.00мин.до17ч.00мин.'],
        ]
        self.facts = [
            [{'from': {'hours': 9, 'minutes': 0}, 'to':  {'hours': 15, 'minutes': 0}}],
            [],
            [{'from': {'hours': 16, 'minutes': 0}, 'to':  {'hours': 17, 'minutes': 0}}],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == interval
        return fact.from_.hours == target_fact['from']['hours'] and \
               fact.from_.minutes == target_fact['from']['minutes'] and \
               fact.to.minutes == target_fact['to']['minutes'] and \
               fact.to.minutes == target_fact['to']['minutes'] and \
               is_valid_type

    def test(self):
        self.base.base_test(Parser(TIME_INTERVAL), self.tests, self.answers, self.facts, self.fact_assertion)


if __name__ == '__main__':
    unittest.main()
