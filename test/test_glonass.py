import unittest

from yargy import Parser

from internal.interpretations import source
from internal.grammatics.source import GLONASS
from test.base import BaseTest


class TestGlonass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base = BaseTest()

    def setUp(self):
        self.tests = [
            '\n[DATE]. поступило сообщение ГЛОНАСС 112 (№) цов  в [ADDRESS], с.[ADDRESS]  скоропостижно скончалась [NAME] [DATE].р. без видимых признаков насильственной смерти.\n',
            '\nСообщение [NAME] "Глонасс+112": двое мужчин распивают спиртное и справляют нужду на детской площадке по [ADDRESS] за магазином "[NAME]". Заявитель не представился.\n',
            '\nВ 03.05 ЧАСОВ [NAME] ,[DATE].Р. [ADDRESS]П.[ADDRESS] ЧЕРЕЗ ДИСПЕТЧЕРА ГЛОНАСС-112 СООБЩИЛА О [NAME],ЧТО ПО [ADDRESS] ХОЗЯИН КАФЕ "[NAME]" ИЗБИВАЕТ ЕЕ.\n',
            '\nГЛОНАСС, В [NAME] [ADDRESS] СОСЕД ИЗ ДОМА №65 [DATE] СЛОМАЛ ВОРОТА ЗАЯВИТЕЛЯ.\n',
            '\nВ 03.05 ЧАСОВ [NAME] ,[DATE].Р. [ADDRESS]П.[ADDRESS] ЧЕРЕЗ ДИСПЕТЧЕРА "ГЛОНАСС-112" СООБЩИЛА О [NAME],ЧТО ПО [ADDRESS] ХОЗЯИН КАФЕ "[NAME]" ИЗБИВАЕТ ЕЕ.\n',
            '\nВ 03.05 ЧАСОВ [NAME] ,[DATE].Р. [ADDRESS]П.[ADDRESS] ЧЕРЕЗ ДИСПЕТЧЕРА СООБЩИЛА О [NAME],ЧТО ПО [ADDRESS] ХОЗЯИН КАФЕ "[NAME]" ИЗБИВАЕТ ЕЕ.\n',
        ]
        self.answers = [
            ['ГЛОНАСС112'],
            ['"Глонасс+112"'],
            ['ГЛОНАСС-112'],
            ['ГЛОНАСС,'],
            ['"ГЛОНАСС-112"'],
            [],
        ]
        self.facts = [
            ['ГЛОНАСС 112'],
            ['Глонасс+112'],
            ['ГЛОНАСС-112'],
            ['ГЛОНАСС,'],
            ['ГЛОНАСС-112'],
            [],
        ]

    @staticmethod
    def fact_assertion(fact, target_fact):
        is_valid_type = type(fact) == source
        return fact.source == target_fact and is_valid_type

    def test(self):
        self.base.base_test(Parser(GLONASS), self.tests, self.answers, self.facts, TestGlonass.fact_assertion)


if __name__ == '__main__':
    unittest.main()
